package ericandmignhe.cs213android35.piece;

import android.content.Context;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;

import ericandmignhe.cs213android35.ChessActivity;
import ericandmignhe.cs213android35.R;
import ericandmignhe.cs213android35.board.Board;

/**
 * 
 * @author Eric and Minghe (Remember to add our names and stuff)
 * Superclass to be extended by individual piece classes.
 *
 */
public class Piece {
	
	/**
	 * IF the piece has moved set to true. Used for checking if castling is possible for king and rook.
	 */
	public boolean hasMoved = false;
	
	/**
	 * Piece's color. 0 - White, 1 - Black
	 */
	public int color;
	
	/**
	 * How the piece appears in string form. 
	 */
	public String representation;

	/**
	 * The physical button object of the piece.
	 */
	public ImageButton pieceButton;

	/**
	 * Is the piece a blank tile or not.
	 */
	public boolean isPiece;

	/**
	 * Default no-arg constructor for piece
	 */
	public Piece() {
		
	}

	/**
	 * Function to check validity of move that a piece is attempting to make.
	 * @param startColumn - Starting column piece is moving from. The file.
	 * @param startRow - Starting row piece is moving from. The rank.
	 * @param endColumn - Ending column pice is moving to. The file.
	 * @param endRow - Ending row piece is moving to. The row.
	 * @param board - Reference to the board the piece is on.
	 * @return boolean if true/false depending on if the move is valid or not.
	 */
	public boolean move(int startColumn, int startRow, int endColumn, int endRow, Piece[][] board, Context c, boolean realMove) {
		return false;
	}
	
	/**
	 * Function to move piece from start coordinates to end coordinates. Called by the move function when a move
	 * is deemed to be a valid move. Not to be used during en passant and castling moves. 
	 * @param startColumn - Starting column piece is moving from. The file.
	 * @param startRow - Starting row piece is moving from. The rank.
	 * @param endColumn - Ending column pice is moving to. The file.
	 * @param endRow - Ending row piece is moving to. The row.
	 * @param board - Reference to the board the piece is on.
	 * @return true, meaning the move has successfully completed.
	 */
	public boolean movePieceOnBoard(int startColumn, int startRow, int endColumn, int endRow, Piece[][] board, Context c, boolean realMove) {

		if (!realMove) {
			//Swapping piece with board tile.
			if (board[endRow][endColumn] instanceof Blank) {
				Piece temp = board[endRow][endColumn];
				board[endRow][endColumn] = board[startRow][startColumn];
				board[startRow][startColumn] = temp;
				return true;
			} else {
				//Capturing piece.
				board[endRow][endColumn] = board[startRow][startColumn];
				board[startRow][startColumn] = new Blank(2, "bL", null);
				return true;
			}
		}

		//I know what follows are two completely different implementations of basically the same code
		//but they both work so I see no current reason to change anything.
		if (board[endRow][endColumn] instanceof Blank) {
			//Piece moves to blank tile
			GridLayout.LayoutParams p1 = new GridLayout.LayoutParams();
			p1.height = dpToPx(40, c);
			p1.width = dpToPx(40, c);
			p1.columnSpec = GridLayout.spec(endColumn);
			p1.rowSpec = GridLayout.spec(endRow);
			board[startRow][startColumn].pieceButton.setLayoutParams(p1);

			GridLayout.LayoutParams p2 = new GridLayout.LayoutParams();
			p2.height = dpToPx(40, c);
			p2.width = dpToPx(40, c);
			p2.columnSpec = GridLayout.spec(startColumn);
			p2.rowSpec = GridLayout.spec(startRow);
			board[endRow][endColumn].pieceButton.setLayoutParams(p2);

			Piece temp = board[endRow][endColumn];
			board[endRow][endColumn] = board[startRow][startColumn];
			board[startRow][startColumn] = temp;
			return true;
		} else {
			ImageButton temp = board[startRow][startColumn].pieceButton;
			board[startRow][startColumn].pieceButton = board[endRow][endColumn].pieceButton;
			if (this instanceof Pawn) {
				if (color == 0) {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.wp);
				} else {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.bp);
				}
			}
			else if (this instanceof Rook) {
				if (color == 0) {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.wr);
				} else {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.br);
				}
			}
			else if (this instanceof Knight) {
				if (color == 0) {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.wn);
				} else {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.bn);
				}
			}
			else if (this instanceof Bishop) {
				if (color == 0) {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.wb);
				} else {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.bb);
				}
			}
			else if (this instanceof Queen) {
				if (color == 0) {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.wq);
				} else {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.bq);
				}
			}
			else if (this instanceof King) {
				if (color == 0) {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.wk);
				} else {
					board[startRow][startColumn].pieceButton.setImageResource(R.drawable.bk);
				}
			}
			Piece blankTile = new Blank(2, "bL", new ImageButton(c));
			temp.setImageResource(0);
			blankTile.pieceButton = temp;

			board[endRow][endColumn] = board[startRow][startColumn];
			board[startRow][startColumn] = blankTile;

			return true;
		}


	}
	
	/**
	 * 
	 * @return The string representation of the piece for the sole use of being printed to the board.
	 */
	public String getStringRepresentation() {
		return representation;
	}
	
	/**
	 * Getter function to return the piece's color.
	 * @return the color of the piece
	 */
	public int getColor() {
		return color;
	}

	public boolean getIsPiece() { return isPiece; }

	public static int dpToPx(int dp, Context context) {
		float density = context.getResources().getDisplayMetrics().density;
		return Math.round((float) dp * density);
	}
	
	
}
