package ericandmignhe.cs213android35.piece;

import android.content.Context;
import android.widget.ImageButton;

import ericandmignhe.cs213android35.ChessActivity;

/**
 * Class for Bishop piece
 * @author Eric Cannone (epc51) and Minghe Hu (mh807)
 *
 */
public class Bishop extends Piece{

	/**
	 * 
	 * @param color - The color of the bishop, 0 for white, and 1 for black.
	 * @param representation - How the bishop appears when displayed on the board. wB - white bishop, or bB - black bishop.
	 */
	public Bishop(int color, String representation, ImageButton b) {
		super();
		this.color = color;
		this.representation = representation;
		pieceButton = b;
		isPiece = true;
	}

	/**
	  * Override of the piece's move method. Specially designed for the rook.
	 */
	public boolean move(int startColumn, int startRow, int endColumn, int endRow, Piece[][] board, Context c, boolean realMove) {
		
		//moving northeast
		if (endColumn > startColumn && endRow < startRow) {
			for (int i = startRow-1, j = startColumn+1; i > endRow && j < endColumn; i--, j++) {
				if (!(board[i][j] instanceof Blank)) {
					return false;
				}
			}
			if (board[endRow][endColumn].getColor() == color) {
				return false;
			}
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//moving northwest
		if (endColumn < startColumn && endRow < startRow) {
			for (int i = startRow-1, j = startColumn-1; i > endRow && j > endColumn; i--, j--) {
				if (!(board[i][j] instanceof Blank)) {
					return false;
				}
			}
			if (board[endRow][endColumn].getColor() == color) {
				return false;
			}
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//moving southwest
		if (endColumn < startColumn && endRow > startRow) {
			for (int i = startRow+1, j = startColumn-1; i < endRow && j > endColumn; i++, j--) {
				if (!(board[i][j] instanceof Blank)) {
					return false;
				}
			}
			if (board[endRow][endColumn].getColor() == color) {
				return false;
			}
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//moving southeast
		if (endColumn > startColumn && endRow > startRow) {
			for (int i = startRow+1, j = startColumn+1; i < endRow && j < endColumn; i++, j++) {
				if (!(board[i][j] instanceof Blank)) {
					return false;
				}
			}
			if (board[endRow][endColumn].getColor() == color) {
				return false;
			}
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		
		return false;
	}
	
}
