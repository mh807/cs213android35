package ericandmignhe.cs213android35.piece;

import android.content.Context;
import android.widget.ImageButton;

import ericandmignhe.cs213android35.ChessActivity;

/**
 * Class for the queen piece
 * @author Eric Cannone (epc51) and Minghe Hu (mh807)
 *
 */
public class Queen extends Piece{

	/**
	 * 
	 * @param color - The color of the queen, 0 for white, and 1 for black.
	 * @param representation - How the queen appears when displayed on the board. wQ - white queen, or bQ - black queen.
	 */
	public Queen(int color, String representation, ImageButton b) {
		super();
		this.color = color;
		this.representation = representation;
		pieceButton = b;
		isPiece = true;
	}
	
	/**
	 * Override of the piece's move method. Specially designed for the queen.
	 */
	public boolean move(int startColumn, int startRow, int endColumn, int endRow, Piece[][] board, Context c, boolean realMove) {
	
		//If the queen attempts to move vertical; looks for pieces in between so it doesn't skip over them; white and black
		if (startColumn == endColumn) {
			if (startRow > endRow) {
				for (int i = endRow+1; i < startRow; i++) {
					if (board[i][endColumn] instanceof Blank)
						continue;
					else {
						return false;
					}
				}
				if (board[endRow][endColumn].getColor() == color) {
					return false;
				}
				return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
			}
			if (endRow > startRow) {
				for (int i = endRow-1; i > startRow; i--) {
					if (board[i][endColumn] instanceof Blank)
						continue;
					else {
						return false;
					}
				}
				if (board[endRow][endColumn].getColor() == color) {
					return false;
				}
				return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
			}
		}
		
		//If the queen attempts to move horizontal; looks for pieces in between so it doesn't skip over them; white and black
		if (startRow == endRow) {
			if (startColumn > endColumn) {
				for (int i = endColumn+1; i < startColumn; i++) {
					if (board[endRow][i] instanceof Blank)
						continue;
					else {
						return false;
					}
				}
				if (board[endRow][endColumn].getColor() == color) {
					return false;
				}
				return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
			}
			if (endColumn> startColumn) {
				for (int i = endColumn-1; i > startColumn; i--) {
					if (board[endRow][i] instanceof Blank)
						continue;
					else {
						return false;
					}
				}
				if (board[endRow][endColumn].getColor() == color) {
					return false;
				}
				return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
			}
		}
				
		//If the queen attempts to move diagonally; looks for pieces in between so it doesn't skip over them; white and black
		//moving northeast
		if (endColumn > startColumn && endRow < startRow) {
			for (int i = startRow-1, j = startColumn+1; i > endRow && j < endColumn; i--, j++) {
				if (!(board[i][j] instanceof Blank)) {
					return false;
				}
			}
			if (board[endRow][endColumn].getColor() == color) {
				return false;
			}
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//moving northwest
		if (endColumn < startColumn && endRow < startRow) {
			for (int i = startRow-1, j = startColumn-1; i > endRow && j > endColumn; i--, j--) {
				if (!(board[i][j] instanceof Blank)) {
					return false;
				}
			}
			if (board[endRow][endColumn].getColor() == color) {
				return false;
			}
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//moving southwest
		if (endColumn < startColumn && endRow > startRow) {
			for (int i = startRow+1, j = startColumn-1; i < endRow && j > endColumn; i++, j--) {
				if (!(board[i][j] instanceof Blank)) {
					return false;
				}
			}
			if (board[endRow][endColumn].getColor() == color) {
				return false;
			}
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//moving southeast
		if (endColumn > startColumn && endRow > startRow) {
			for (int i = startRow+1, j = startColumn+1; i < endRow && j < endColumn; i++, j++) {
				if (!(board[i][j] instanceof Blank)) {
					return false;
				}
			}
			if (board[endRow][endColumn].getColor() == color) {
				return false;
			}
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		return false;
	}
}
