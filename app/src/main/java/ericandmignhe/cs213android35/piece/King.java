package ericandmignhe.cs213android35.piece;

import android.content.Context;
import android.widget.ImageButton;

import ericandmignhe.cs213android35.ChessActivity;

/**
 * Class for the King piece
 * @author Eric Cannone (epc51) and Minghe Hu (mh807)
 *
 */
public class King extends Piece {

	/**
	 * 
	 * @param color - The color of the king, 0 for white, and 1 for black.
	 * @param representation - How the king appears when displayed on the board. wK - white king, or bK - black king.
	 */
	public King(int color, String representation, ImageButton b) {
		super();
		this.color = color;
		this.representation = representation;
		pieceButton = b;
		isPiece = true;
	}

	/**
	 * Override of the piece's move method. Specially designed for the King.
	 */
	public boolean move(int startColumn, int startRow, int endColumn, int endRow, Piece[][] board, Context c, boolean realMove) {
		//castling conditionals
		if (hasMoved == false) {
			if (startRow == endRow && endColumn == startColumn + 2 && board[startRow][startColumn+1] instanceof Blank && board[startRow][startColumn+2] instanceof Blank
					&& board[startRow][startColumn+3] instanceof Rook && board[startRow][startColumn+3].hasMoved == false) {
				board[startRow][startColumn+3].hasMoved = true;
				movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove); //move the king
				movePieceOnBoard(startColumn+3, startRow, endColumn-1, endRow, board, c, realMove); //move the rook
				hasMoved = true;
				return true;
				
			}
			else if (startRow == endRow && endColumn == startColumn - 2 && board[startRow][startColumn-1] instanceof Blank && board[startRow][startColumn-2] instanceof Blank
					&& board[startRow][startColumn-3] instanceof Blank && board[startRow][startColumn-4] instanceof Rook
					&& board[startRow][startColumn-4].hasMoved == false) {
				board[startRow][startColumn-4].hasMoved = true;
				movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove); //move the king
				movePieceOnBoard(startColumn-4, startRow, endColumn+1, endRow, board, c, realMove); //move the rook
				hasMoved = true;
				return true;
				
			}
		}

		//If the king attempts to move vertically by 1 space; white
		if (Math.abs(endRow - startRow) == 1 && startColumn == endColumn && board[endRow][endColumn].getColor() != color) {
			hasMoved = true;
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
				
		//If the king attempts to move horizontally by 1 space; white
		if (Math.abs(endColumn - startColumn) == 1 && startRow == endRow && board[endRow][endColumn].getColor() != color) {
			hasMoved = true;
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//If the king attempts to move diagonally by 1 space; white
		if (Math.abs(endColumn - startColumn) == 1 && Math.abs(endRow - startRow) == 1 && board[endRow][endColumn].getColor() != color) {
			hasMoved = true;
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
			
		return false;
	}
}
