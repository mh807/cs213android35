package ericandmignhe.cs213android35.piece;

import android.content.Context;
import android.widget.GridLayout;
import android.widget.ImageButton;

import ericandmignhe.cs213android35.Chess;
import ericandmignhe.cs213android35.ChessActivity;
import ericandmignhe.cs213android35.R;

/**
 * Class for pawn piece. Overrides behaviors for move method and more.
 * @author Eric Cannone (epc51) and Minghe Hu
 *
 */
public class Pawn extends Piece {
	
	public boolean recentlyDoubleMoved = false;
	
	/**
	 * 
	 * @param color - The color of the pawn, 0 for white, and 1 for black.
	 * @param representation - How the pawn appears when displayed on the board. wp - white pawn, or bp - black pawn.
	 */
	public Pawn(int color, String representation, ImageButton b) {
		super();
		this.color = color;
		this.representation = representation;
		pieceButton = b;
        isPiece = true;
	}
	
	/**
	 * Override of the piece's move method. Specially designed for the pawn.
	 */
	public boolean move(int startColumn, int startRow, int endColumn, int endRow, Piece[][] board, Context c, boolean realMove) {
		
		//Pawn is attempting to perform an en passant, white
		if (color == 0 && endRow - startRow == -1 && Math.abs(startColumn - endColumn) == 1 && board[endRow][endColumn] instanceof Blank
				&& board[endRow+1][endColumn] != null && board[endRow+1][endColumn].getColor() != color && board[endRow+1][endColumn] == (Piece)Chess.recentlyDoubleMovedPawnQueue.peek()) {

			enPassant(startColumn, startRow, endColumn, endRow, board, c, realMove);
			return true;
		}
		
		//black
		if (color == 1 && endRow - startRow == 1 && Math.abs(startColumn - endColumn) == 1 && board[endRow][endColumn] instanceof Blank
				&& board[endRow-1][endColumn] != null && board[endRow-1][endColumn].getColor() != color && board[endRow-1][endColumn] == (Piece)Chess.recentlyDoubleMovedPawnQueue.peek()) {

			enPassant(startColumn, startRow, endColumn, endRow, board, c, realMove);
			return true;
		}
		
		//If the pawn attempts a double move, white
		if (color == 0 && endRow - startRow == -2 && startColumn == endColumn && board[endRow][endColumn] instanceof Blank & startRow == 6) {
			if (!(board[startRow-1][startColumn] instanceof Blank)) {
				return false;
			}
			recentlyDoubleMoved = true;
			Chess.recentlyDoubleMovedPawnQueue.add((Pawn)board[startRow][startColumn]);
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//black
		if (color == 1 && endRow - startRow == 2 && startColumn == endColumn && board[endRow][endColumn] instanceof Blank & startRow == 1) {
			if (!(board[startRow+1][startColumn] instanceof Blank)) {
				return false;
			}
			recentlyDoubleMoved = true;
			Chess.recentlyDoubleMovedPawnQueue.add((Pawn)board[startRow][startColumn]);
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		
		//If the pawn attempts to move forward 1 space, white
		if (color == 0 && endRow - startRow == -1 && startColumn == endColumn && board[endRow][endColumn] instanceof Blank) {
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//black
		if (color == 1 && endRow - startRow == 1 && startColumn == endColumn && board[endRow][endColumn] instanceof Blank) {
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//if the pawn attempts to capture diagonally, white
		if (color == 0 && endRow - startRow == -1 && Math.abs(startColumn - endColumn) == 1 && !(board[endRow][endColumn] instanceof Blank) && board[endRow][endColumn].getColor() != color) {
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		//black
		if (color == 1 && endRow - startRow == 1 && Math.abs(startColumn - endColumn) == 1 && !(board[endRow][endColumn] instanceof Blank) && board[endRow][endColumn].getColor() != color) {
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		return false;
	}
	
	/**
	 * To be called after the window to en passant on this pawn has ended.
	 * Honestly why do I even add these functions, I set all the instance variable to public lol.
	 */
	public void setRecentlyDoubleMovedToFalse() {
		recentlyDoubleMoved = false;
	}

	/**
	 * Special move function for en passant only. Similar to movePieceOnBoard but slightly different to accomodate strange nature of en passant.
	 */
	public void enPassant(int startColumn, int startRow, int endColumn, int endRow, Piece[][] board, Context c, boolean realMove) {
		if (!realMove) {
			Piece temp = board[endRow][endColumn];
			board[endRow][endColumn] = board[startRow][startColumn];
			board[startRow][startColumn] = temp;
			if (color == 0) {
				board[endRow+1][endColumn] = new Blank(2, "bL", null);
			} else {
				board[endRow-1][endColumn] = new Blank(2, "bL", null);
			}
			return;
		} else {
			ImageButton temp = board[startRow][startColumn].pieceButton;
			ImageButton capturedTile;
			if (color == 0) {
				capturedTile = board[endRow+1][endColumn].pieceButton;
			} else {
				capturedTile = board[endRow-1][endColumn].pieceButton;
			}
			board[startRow][startColumn].pieceButton = board[endRow][endColumn].pieceButton;
			if (color == 0) {
				board[startRow][startColumn].pieceButton.setImageResource(R.drawable.wp);
			} else {
				board[startRow][startColumn].pieceButton.setImageResource(R.drawable.bp);
			}
			temp.setImageResource(0);
			capturedTile.setImageResource(0);

			//moves all pieces on logical board
			Piece bl = board[endRow][endColumn];
			board[endRow][endColumn] = board[startRow][startColumn];
			board[startRow][startColumn] = bl;
			Piece blankTile = new Blank(2, "bL", new ImageButton(c));
			if (color == 0) {
				board[endRow+1][endColumn] = blankTile;
			} else {
				board[endRow-1][endColumn] = blankTile;
			}
			bl.pieceButton = temp;
			blankTile.pieceButton = capturedTile;
			return;
		}
	}
	
	
	
}
