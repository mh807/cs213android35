package ericandmignhe.cs213android35.piece;

import android.content.Context;
import android.widget.ImageButton;

import ericandmignhe.cs213android35.ChessActivity;

/**
 * Class for knight piece.
 * @author Eric Cannone (epc51) and Minghe Hu (mh807)
 *
 */
public class Knight extends Piece{

	/**
	 * 
	 * @param color - The color of the knight, 0 for white, and 1 for black.
	 * @param representation - How the knight appears when displayed on the board. wN - white knight, or bN - black knight.
	 */
	public Knight(int color, String representation, ImageButton b) {
		super();
		this.color = color;
		this.representation = representation;
		pieceButton = b;
		isPiece = true;
	}
	
	/**
	 * Override of the piece's move method. Specially designed for the knight.
	 */
	public boolean move(int startColumn, int startRow, int endColumn, int endRow, Piece[][] board, Context c, boolean realMove) {
	
		//If the knight attempts to move; white
		if (Math.abs(endRow - startRow) == 1 && Math.abs(endColumn - startColumn) == 2 && board[endRow][endColumn].getColor() != color) {
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
		
		if (Math.abs(endRow - startRow) == 2 && Math.abs(endColumn - startColumn) == 1 && board[endRow][endColumn].getColor() != color) {
			return movePieceOnBoard(startColumn, startRow, endColumn, endRow, board, c, realMove);
		}
				
		return false;
	}

}
