package ericandmignhe.cs213android35.piece;

import android.widget.ImageButton;

/**
 * Created by Eric on 4/29/2017.
 */

/**
 * This class will be used to denote blank tiles.
 */
public class Blank extends Piece {

    /**
     * Default no-arg constructor for piece
     */
    public Blank(int c, String sr, ImageButton b) {
        color = c;
        representation = sr;
        pieceButton = b;
        isPiece = false;

    }

}
