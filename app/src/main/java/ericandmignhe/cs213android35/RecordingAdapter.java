package ericandmignhe.cs213android35;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Eric on 5/1/2017.
 */

public class RecordingAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Recording> mDataSource;

    public RecordingAdapter(Context context, ArrayList<Recording> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get view for row item
        View rowView = mInflater.inflate(R.layout.recording_list_item, parent, false);

        TextView titleTextView = (TextView) rowView.findViewById(R.id.recording_list_title);
        TextView subtitleTextView = (TextView) rowView.findViewById(R.id.recording_list_subtitle);

        Recording game = (Recording) getItem(position);

        titleTextView.setText(game.name);
        subtitleTextView.setText(game.date);

        return rowView;
    }

}
