package ericandmignhe.cs213android35;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import ericandmignhe.cs213android35.piece.*;

import ericandmignhe.cs213android35.board.Board;
import ericandmignhe.cs213android35.piece.*;

/**
 * Main class for chess. This is where the main chess loop runs.
 * @author Eric and Minghe
 *
 */
public class Chess {
	
	/**
	 * Flag that if enabled causes the game to end.
	 */
	public static boolean isGameOver = false;
	
	/**
	 * Queue for storing pawns that can be en passanted upon.
	 */
	public static Queue<Pawn> recentlyDoubleMovedPawnQueue = new LinkedList<Pawn>();
	
	/**
	 * Boolean so that pawns just added to the queue are not immediately removed when the moving players turn ends.
	 */
	public static boolean pawnQueueWasEmptyLastTurn = true;
	
	/**
	 * If a pawn has been in the queue for 1 complete opponent's turns, remove it from the queue.
	 */
	public static void endWindowToPerformEnPassant() {
		if (!pawnQueueWasEmptyLastTurn) {
			Pawn p = recentlyDoubleMovedPawnQueue.poll();
			p.setRecentlyDoubleMovedToFalse();
		}
		
		if (recentlyDoubleMovedPawnQueue.isEmpty()) {
			pawnQueueWasEmptyLastTurn = true;
		} else {
			pawnQueueWasEmptyLastTurn = false;
		}
	}


}
