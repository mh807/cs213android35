package ericandmignhe.cs213android35;

import java.util.ArrayList;
import java.util.Random;

import ericandmignhe.cs213android35.piece.Bishop;
import ericandmignhe.cs213android35.piece.Blank;
import ericandmignhe.cs213android35.piece.King;
import ericandmignhe.cs213android35.piece.Knight;
import ericandmignhe.cs213android35.piece.Pawn;
import ericandmignhe.cs213android35.piece.Piece;
import ericandmignhe.cs213android35.piece.Queen;
import ericandmignhe.cs213android35.piece.Rook;

/**
 * Created by Eric on 4/30/2017.
 * This class is basically a shameless copy of Board with some tweaked values. I'm pretty lazy and I got
 * other classes to study for. So its good enough.
 */
public class AI {
    
    Piece[][] board;
    ChessActivity ca;
    int[] whiteKingLocation = new int[2];
    int[] blackKingLocation = new int[2];
    
    public int[] findFirstAvailableMove(int color, Piece [][] b, ChessActivity c, int[] wkl, int[] bkl) {
        ca = c;
        board = b;
        whiteKingLocation = wkl;
        blackKingLocation = bkl;
        /*
        if (canTheKingMakeAValidMove(color)) {
            return false;
        }
        */

        Piece[][] boardcopy = copyBoard(board);

        ArrayList<int []> moves = new ArrayList<>();

        //attempt to make a move with everyone single one of the the colors piece.
        for (int i = 0; i < boardcopy.length; i++) {
            for (int j = 0; j < boardcopy[0].length; j++) {
                if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Pawn) {
                    if (color == 0) {
                        if (i != 0 && attemptMove(j, i, j, i-1, color)) {
                            moves.add(new int []{j,i,j,i-1});
                        }
                        if (i != 0 && j != 0 && attemptMove(j, i, j-1, i-1, color)) {
                            moves.add(new int []{j, i, j-1, i-1});
                        }
                        if (i != 0 && j != 7 && attemptMove(j, i, j+1, i-1, color)) {
                            moves.add(new int []{j, i, j+1, i-1});
                        }
                    } else {
                        if (i != 7 && attemptMove(j, i, j, i+1, color)) {
                            moves.add(new int []{j, i, j, i+1});
                        }
                        if (i != 7 && j != 0 && attemptMove(j, i, j-1, i+1, color)) {
                            moves.add(new int []{j, i, j-1, i+1});
                        }
                        if (i != 0 && j != 7 && attemptMove(j, i, j+1, i+1, color)) {
                            moves.add(new int []{j, i, j+1, i+1});
                        }
                    }
                }
                else if (boardcopy[i][j] != null &&  boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Rook) {
                    if (i != 0 && attemptMove(j, i, j, i-1, color)) {
                        moves.add(new int []{j, i, j, i-1});
                    }
                    if (i != 7 && attemptMove(j, i, j, i+1, color)) {
                        moves.add(new int []{j, i, j, i+1});
                    }
                    if (j != 0 && attemptMove(j, i, j-1, i, color)) {
                        moves.add(new int []{j, i, j-1, i});
                    }
                    if (j != 7 && attemptMove(j, i, j+1, i, color)) {
                        moves.add(new int []{j, i, j+1, i});
                    }
                }
                else if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Bishop) {
                    if (i != 0 && j != 7  && attemptMove(j, i, j+1, i-1, color)) {
                        moves.add(new int []{j, i, j+1, i-1});
                    }
                    if (i != 0 && j != 0  && attemptMove(j, i, j-1, i-1, color)) {
                        moves.add(new int []{j, i, j-1, i-1});
                    }
                    if (i != 7 && j != 0  && attemptMove(j, i, j-1, i+1, color)) {
                        moves.add(new int []{j, i, j-1, i+1});
                    }
                    if (i != 7 && j != 7  && attemptMove(j, i, j+1, i+1, color)) {
                        moves.add(new int []{j, i, j+1, i+1});
                    }
                }
                else if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Queen) {
                    if (i != 0 && attemptMove(j, i, j, i-1, color)) {
                        moves.add(new int []{j, i, j, i-1});
                    }
                    if (i != 7 && attemptMove(j, i, j, i+1, color)) {
                        moves.add(new int []{j, i, j, i+1});
                    }
                    if (j != 0 && attemptMove(j, i, j-1, i, color)) {
                        moves.add(new int []{j, i, j-1, i});
                    }
                    if (j != 7 && attemptMove(j, i, j+1, i, color)) {
                        moves.add(new int []{j, i, j+1, i});
                    }
                    if (i != 0 && j != 7  && attemptMove(j, i, j+1, i-1, color)) {
                        moves.add(new int []{j, i, j+1, i-1});
                    }
                    if (i != 0 && j != 0  && attemptMove(j, i, j-1, i-1, color)) {
                        moves.add(new int []{j, i, j-1, i-1});
                    }
                    if (i != 7 && j != 0  && attemptMove(j, i, j-1, i+1, color)) {
                        moves.add(new int []{j, i, j-1, i+1});
                    }
                    if (i != 7 && j != 7  && attemptMove(j, i, j+1, i+1, color)) {
                        moves.add(new int []{j, i, j+1, i+1});
                    }
                }
                else if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Knight) {
                    if (i + 1 < 8 && j + 2 < 8 && attemptMove(j, i, j+2, i+1, color)) {
                        moves.add(new int []{j, i, j+2, i+1});
                    }

                    if (i + 1 < 8 && j - 2 >= 0 && attemptMove(j, i, j-2, i+1, color)) {
                        moves.add(new int []{j, i, j-2, i+1});
                    }

                    if (i + 2 < 8 && j + 1 < 8 && attemptMove(j, i, j+1, i+2, color)) {
                        moves.add(new int []{j, i, j+1, i+2});
                    }

                    if (i + 2 < 8 && j - 1 >= 0 && attemptMove(j, i, j-1, i+2, color)) {
                        moves.add(new int []{j, i, j-1, i+2});
                    }

                    if (i - 1 >= 0 && j + 2 < 8 && attemptMove(j, i, j+2, i-1, color)) {
                        moves.add(new int []{j, i, j+2, i-1});
                    }

                    if (i - 1 >= 0 && j - 2 >= 0 && attemptMove(j, i, j-2, i-1, color)) {
                        moves.add(new int []{j, i, j-2, i-1});
                    }

                    if (i - 2 >= 0 && j + 1 < 8 && attemptMove(j, i, j+1, i-2, color)) {
                        moves.add(new int []{j, i, j+1, i-2});
                    }

                    if (i - 2 >= 0 && j - 1 >= 0 && attemptMove(j, i, j-1, i-2, color)) {
                        moves.add(new int []{j, i, j-1, i-2});
                    }
                }
                else if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof King) {
                    if (i - 1 >= 0 && j - 1 >= 0 && attemptMove(j, i, j - 1, i - 1, color)) {
                        moves.add(new int []{j, i, j - 1, i - 1});
                    }

                    if (i - 1 >= 0 && attemptMove(j, i, j, i - 1, color)) {
                        moves.add(new int []{j, i, j, i - 1});
                    }

                    if (i - 1 >= 0 && j + 1 < 8 && attemptMove(j, i, j + 1, i - 1, color)) {
                        moves.add(new int []{j, i, j + 1, i - 1});
                    }

                    if (j - 1 >= 0 && attemptMove(j, i, j - 1, i, color)) {
                        moves.add(new int []{j, i, j - 1, i});
                    }

                    if (j + 1 < 8 && attemptMove(j, i, j + 1, i, color)) {
                        moves.add(new int []{j, i, j + 1, i});
                    }

                    if (i + 1 < 8 && j - 1 >= 0 && attemptMove(j, i, j - 1, i + 1, color)) {
                        moves.add(new int []{j, i, j - 1, i + 1});
                    }

                    if (i + 1 < 8 && attemptMove(j, i, j, i + 1, color)) {
                        moves.add(new int []{j, i, j, i + 1});
                    }

                    if (i + 1 < 8 && j + 1 < 8 && attemptMove(j, i, j + 1, i + 1, color)) {
                        moves.add(new int []{j, i, j + 1, i + 1});
                    }
                }
            }
        }

        if (moves.size() > 0) {
            Random rand = new Random();
            int value = rand.nextInt(moves.size());
            return moves.get(value);
        }

        return null;
    }

    public boolean checkIfKingIsInCheck(int color, Piece[][] board, boolean checkingForKing) {
        if (checkingForKing) {
            int kingColumn;
            int kingRow;
            if (color == 0) {
                kingColumn = whiteKingLocation[0];
                kingRow = whiteKingLocation[1];
            } else {
                kingColumn = blackKingLocation[0];
                kingRow = blackKingLocation[1];
            }
            return checkIfKingIsInCheck(color, board, kingColumn, kingRow);
        } else {
            //return checkIfKingIsInCheck(color, board, checkingPieceLocation[0], checkingPieceLocation[1]);
            return true;
        }
    }

    public boolean attemptMove(int startColumn, int startRow, int endColumn, int endRow, int color) {
        Piece[][] boardcopy = copyBoard(board);

        //This only checks for moving the king.
        if (boardcopy[startRow][startColumn] instanceof King) {
            if (boardcopy[startRow][startColumn].move(startColumn, startRow, endColumn, endRow, boardcopy, ca, false) && !checkIfKingIsInCheck(color, boardcopy, endColumn, endRow)) {
                return true;
            }
        }
        //If we're trying to move a piece besides the king.
        else {
            if (boardcopy[startRow][startColumn].move(startColumn, startRow, endColumn, endRow, boardcopy, ca, false) && !checkIfKingIsInCheck(color, boardcopy, true)) {
                return true;
            }
        }

        return false;
    }

    public Piece[][] copyBoard(Piece [][] board) {
        Piece[][] boardcopy = new Piece[8][8];

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] instanceof Rook) {
                    boardcopy[i][j] = new Rook(board[i][j].getColor(), board[i][j].getStringRepresentation(), null);
                }
                else if (board[i][j] instanceof King) {
                    boardcopy[i][j] = new King(board[i][j].getColor(), board[i][j].getStringRepresentation(), null);
                } else {
                    boardcopy[i][j] = board[i][j];
                }
            }
        }

        return boardcopy;
    }

    public boolean checkIfKingIsInCheck(int color, Piece [][] board, int kingColumn, int kingRow) {

        //check for attacking rooks or queens from the south
        for (int i = kingRow+1; i < board.length; i++) {
            if (!(board[i][kingColumn] instanceof Blank)) {
                if (board[i][kingColumn].getColor() != color && (board[i][kingColumn] instanceof Rook || board[i][kingColumn] instanceof Queen)) {
                    return true;
                } else {
                    break;
                }
            }
        }

        //check for attacking rooks or queens from the north ("The north remembers" -Ned Stark)
        for (int i = kingRow-1; i >= 0; i--) {
            if (!(board[i][kingColumn] instanceof Blank)) {
                if (board[i][kingColumn].getColor() != color && (board[i][kingColumn] instanceof Rook || board[i][kingColumn] instanceof Queen)) {
                    return true;
                } else {
                    break;
                }
            }
        }

        //check for attacking rooks or queens from the east
        for (int i = kingColumn+1; i < board.length; i++) {
            if (!(board[kingRow][i] instanceof Blank)) {
                if (board[kingRow][i].getColor() != color && (board[kingRow][i] instanceof Rook || board[kingRow][i] instanceof Queen)) {
                    return true;
                } else {
                    break;
                }
            }
        }

        //check for attacking rooks or queens from the west
        for (int i = kingColumn-1; i >= 0; i--) {
            if (!(board[kingRow][i] instanceof Blank)) {
                if (board[kingRow][i].getColor() != color && (board[kingRow][i] instanceof Rook || board[kingRow][i] instanceof Queen)) {
                    return true;
                } else {
                    break;
                }
            }
        }

        //check for knights of the old republic III, all 8 possible variations of where a knight can be placed in relation to a king.
        if (kingRow + 1 < 8 && kingColumn + 2 < 8 && board[kingRow+1][kingColumn+2] != null && board[kingRow+1][kingColumn+2].getColor() != color && board[kingRow+1][kingColumn+2] instanceof Knight) {
            return true;
        }

        if (kingRow + 1 < 8 && kingColumn - 2 >= 0 && board[kingRow+1][kingColumn-2] != null && board[kingRow+1][kingColumn-2].getColor() != color && board[kingRow+1][kingColumn-2] instanceof Knight) {
            return true;
        }

        if (kingRow + 2 < 8 && kingColumn + 1 < 8 && board[kingRow+2][kingColumn+1] != null && board[kingRow+2][kingColumn+1].getColor() != color && board[kingRow+2][kingColumn+1] instanceof Knight) {
            return true;
        }

        if (kingRow + 2 < 8 && kingColumn - 1 >= 0 && board[kingRow+2][kingColumn-1] != null && board[kingRow+2][kingColumn-1].getColor() != color && board[kingRow+2][kingColumn-1] instanceof Knight) {
            return true;
        }

        if (kingRow - 1 >= 0 && kingColumn + 2 < 8 && board[kingRow-1][kingColumn+2] != null && board[kingRow-1][kingColumn+2].getColor() != color && board[kingRow-1][kingColumn+2] instanceof Knight) {
            return true;
        }

        if (kingRow - 1 >= 0 && kingColumn - 2 >= 0 && board[kingRow-1][kingColumn-2] != null && board[kingRow-1][kingColumn-2].getColor() != color && board[kingRow-1][kingColumn-2] instanceof Knight) {
            return true;
        }

        if (kingRow - 2 >= 0 && kingColumn + 1 < 8 && board[kingRow-2][kingColumn+1] != null && board[kingRow-2][kingColumn+1].getColor() != color && board[kingRow-2][kingColumn+1] instanceof Knight) {
            return true;
        }

        if (kingRow - 2 >= 0 && kingColumn - 1 >= 0 && board[kingRow-2][kingColumn-1] != null && board[kingRow-2][kingColumn-1].getColor() != color && board[kingRow-2][kingColumn-1] instanceof Knight) {
            return true;
        }
        //end knight checks

        //check for bishops and queens attacking from southeast
        for (int i = kingRow+1, j = kingColumn+1; i < board.length && j < board.length; i++, j++) {
            if (!(board[i][j] instanceof Blank)) {
                if (board[i][j].getColor() != color && (board[i][j] instanceof Bishop || board[i][j] instanceof Queen)) {
                    return true;
                } else {
                    break;
                }
            }
        }

        //check for bishops and queens attacking from southwest
        for (int i = kingRow+1, j = kingColumn-1; i < board.length && j >= 0; i++, j--) {
            if (!(board[i][j] instanceof Blank)) {
                if (board[i][j].getColor() != color && (board[i][j] instanceof Bishop || board[i][j] instanceof Queen)) {
                    return true;
                } else {
                    break;
                }
            }
        }

        //check for bishops and queens attacking from northeast
        for (int i = kingRow-1, j = kingColumn+1; i >= 0 && j < board.length; i--, j++) {
            if (!(board[i][j] instanceof Blank)) {
                if (board[i][j].getColor() != color && (board[i][j] instanceof Bishop || board[i][j] instanceof Queen)) {
                    return true;
                } else {
                    break;
                }
            }
        }


        //check for bishops and queens attacking from northwest
        for (int i = kingRow-1, j = kingColumn-1; i >= 0 && j >= 0; i--, j--) {
            if (!(board[i][j] instanceof Blank)) {
                if (board[i][j].getColor() != color && (board[i][j] instanceof Bishop || board[i][j] instanceof Queen)) {
                    return true;
                } else {
                    break;
                }
            }
        }

        //Check for attacking pawns (if king is white and pawns are black) (northwest) (also known as the lamest way to get put into check)
        if (color == 0 && kingRow-1 >= 0 && kingColumn-1 >= 0 && board[kingRow-1][kingColumn-1] != null && board[kingRow-1][kingColumn-1].getColor() != color && board[kingRow-1][kingColumn-1] instanceof Pawn) {
            return true;
        }

        //Check for attacking pawns (if king is white and pawns are black) (northeast)
        if (color == 0 && kingRow-1 >= 0 && kingColumn+1 < 8 && board[kingRow-1][kingColumn+1] != null && board[kingRow-1][kingColumn+1].getColor() != color && board[kingRow-1][kingColumn+1] instanceof Pawn) {
            return true;
        }

        //Check for attacking pawns (if king is black and pawns are white) (southwest)
        if (color == 1 && kingRow+1 < 8 && kingColumn-1 >= 0 && board[kingRow+1][kingColumn-1] != null && board[kingRow+1][kingColumn-1].getColor() != color && board[kingRow+1][kingColumn-1] instanceof Pawn) {
            return true;
        }

        //Check for attacking pawns (if king is black and pawns are white) (southeast)
        if (color == 1 && kingRow+1 < 8 && kingColumn+1 < 8 && board[kingRow+1][kingColumn+1] != null && board[kingRow+1][kingColumn+1].getColor() != color && board[kingRow+1][kingColumn+1] instanceof Pawn) {
            return true;
        }

        //Check for attacking king (NOTE: In normal play this cannot happen. A king cannot put another king in check without putting itself in check.
        //However this is useful for preemptively checking if moving your king will put it in check with another king. (I.E. illegal move)).
        if (kingRow - 1 >= 0 && kingColumn-1 >= 0 && board[kingRow-1][kingColumn-1] != null && board[kingRow-1][kingColumn-1].getColor() != color && board[kingRow-1][kingColumn-1] instanceof King) {
            return true;
        }

        if (kingRow - 1 >= 0 && board[kingRow-1][kingColumn] != null && board[kingRow-1][kingColumn].getColor() != color && board[kingRow-1][kingColumn] instanceof King) {
            return true;
        }

        if (kingRow - 1 >= 0 && kingColumn+1 < 8 && board[kingRow-1][kingColumn+1] != null && board[kingRow-1][kingColumn+1].getColor() != color && board[kingRow-1][kingColumn+1] instanceof King) {
            return true;
        }

        if (kingColumn-1 >= 0 && board[kingRow][kingColumn-1] != null && board[kingRow][kingColumn-1].getColor() != color && board[kingRow][kingColumn-1] instanceof King) {
            return true;
        }

        if (kingColumn+1 < 8 && board[kingRow][kingColumn+1] != null && board[kingRow][kingColumn+1].getColor() != color && board[kingRow][kingColumn+1] instanceof King) {
            return true;
        }

        if (kingRow + 1 < 8 && kingColumn-1 >= 0 && board[kingRow+1][kingColumn-1] != null && board[kingRow+1][kingColumn-1].getColor() != color && board[kingRow+1][kingColumn-1] instanceof King) {
            return true;
        }

        if (kingRow + 1 < 8 && board[kingRow+1][kingColumn] != null && board[kingRow+1][kingColumn].getColor() != color && board[kingRow+1][kingColumn] instanceof King) {
            return true;
        }

        if (kingRow + 1 < 8 && kingColumn+1 < 8 && board[kingRow+1][kingColumn+1] != null && board[kingRow+1][kingColumn+1].getColor() != color && board[kingRow+1][kingColumn+1] instanceof King) {
            return true;
        }
        //end king checks


        return false;
    }
}
