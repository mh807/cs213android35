package ericandmignhe.cs213android35.board;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ImageButton;

import ericandmignhe.cs213android35.Chess;
import ericandmignhe.cs213android35.ChessActivity;
import ericandmignhe.cs213android35.R;
import ericandmignhe.cs213android35.piece.*;

/**
 * Board creates the chess board as well as stores all the pieces on the board.
 * @author Eric and Minghe
 *
 */
public class Board {

	/**
	 * Chess activity that will get passed around. I added this in much later than I should have.
	 */
	Context ca;
	
	/**
	 * Board is a 2-dimensional array of pieces.
	 */
	public Piece[][] board;
	
	/**
	 * Array of size 2 which tracks white king's file and rank position on the chessboard.
	 * Main purpose of this array is for when it is necessary to check if a king is in check.
	 * Anytime a king is moved its position in this array should be updated.
	 * Index 0 - File/Column, represented by letters (but converted to int for ease of use purposes).
	 * Index 1 - Rank/Row, represented by numbers.
	 */
	public int[] whiteKingLocation = new int[2];
	
	/**
	 * Array of size 2 which tracks black king's file and rank position on the chessboard.
	 */
	public int[] blackKingLocation = new int[2];
	
	/**
	 * Array of size which tracks the position of the last piece to place a king in check.
	 * Used for when checking for checkmate.
	 */
	public int[] checkingPieceLocation = new int[2];
	
	/**
	 * Flag set to true if player offers another player a draw. If that draw offer is rejected the flag gets reset to false.
	 */
	public boolean drawOffered = false;
	
	/**
	 * No-arg constructor for board. Creates a new board object, initializes the board array.
	 */
	public Board(Context c){
		ca = c;
		board = new Piece[8][8];
	}

	/**
	 * Ends the game and prints the victor.
	 * @param color - color who won
	 */
	public void endGame(int color) {
		Chess.isGameOver = true;
		if (color == 0) {
			System.out.println();
			System.out.println("White wins");
		}
		else {
			System.out.println();
			System.out.println("Black wins");
		}
	}

    public void createNewGame(Board chessBoard, Context c) {
		View rootView = ((Activity)c).getWindow().getDecorView().findViewById(android.R.id.content);
        for (int i = 0; i < chessBoard.board.length; i++) {
            if (i == 0) {
                chessBoard.board[0][i] = new Rook(1, "bR", (ImageButton)rootView.findViewById(R.id.br1));
            } else if (i == 7) {
                chessBoard.board[0][i] = new Rook(1, "bR", (ImageButton)rootView.findViewById(R.id.br2));
            }
            else if (i == 1) {
                chessBoard.board[0][i] = new Knight(1, "bN", (ImageButton)rootView.findViewById(R.id.bn1));
            }
            else if (i == 6) {
                chessBoard.board[0][i] = new Knight(1, "bN", (ImageButton)rootView.findViewById(R.id.bn2));
            }
            else if (i == 2) {
                chessBoard.board[0][i] = new Bishop(1, "bB", (ImageButton)rootView.findViewById(R.id.bb1));
            }
            else if (i == 5) {
                chessBoard.board[0][i] = new Bishop(1, "bB", (ImageButton)rootView.findViewById(R.id.bb2));
            }
            else if (i == 3) {
                chessBoard.board[0][i] = new Queen(1, "bQ", (ImageButton)rootView.findViewById(R.id.bq));
            }
            else if (i == 4) {
                chessBoard.board[0][i] = new King(1, "bK", (ImageButton)rootView.findViewById(R.id.bk));
            }
        }
        int id = 1;
        for (int i = 0; i < chessBoard.board.length; i++) {
            String buttonID = "bp" + id;
            int resID = c.getResources().getIdentifier(buttonID, "id", c.getPackageName());
            chessBoard.board[1][i] = new Pawn(1, "bp", (ImageButton)rootView.findViewById(resID));
            id++;
        }
        //Add blank tiles now.
        id = 1;
        for (int i = 2; i < 6; i++) {
            for (int j = 0; j < chessBoard.board[i].length; j++) {
                String buttonID = "bl" + id;
                int resID = c.getResources().getIdentifier(buttonID, "id", c.getPackageName());
                chessBoard.board[i][j] = new Blank(2, "bT", (ImageButton)rootView.findViewById(resID));
                id++;
            }
        }

        for (int i = 0; i < chessBoard.board.length; i++) {
            if (i == 0) {
                chessBoard.board[7][i] = new Rook(0, "wR", (ImageButton)rootView.findViewById(R.id.wr1));
            }
            else if (i == 7) {
                chessBoard.board[7][i] = new Rook(0, "wR", (ImageButton)rootView.findViewById(R.id.wr2));
            }
            else if (i == 1) {
                chessBoard.board[7][i] = new Knight(0, "wN", (ImageButton)rootView.findViewById(R.id.wn1));
            }
            else if (i == 6) {
                chessBoard.board[7][i] = new Knight(0, "wN", (ImageButton)rootView.findViewById(R.id.wn2));
            }
            else if (i == 2) {
                chessBoard.board[7][i] = new Bishop(0, "wB", (ImageButton)rootView.findViewById(R.id.wb1));
            }
            else if (i== 5) {
                chessBoard.board[7][i] = new Bishop(0, "wB", (ImageButton)rootView.findViewById(R.id.wb2));
            }
            else if (i == 3) {
                chessBoard.board[7][i] = new Queen(0, "wQ", (ImageButton)rootView.findViewById(R.id.wq));
            }
            else if (i == 4) {
                chessBoard.board[7][i] = new King(0, "wK", (ImageButton)rootView.findViewById(R.id.wk));
            }
        }

        id=1;
        for (int i = 0; i < chessBoard.board.length; i++) {
            String buttonID = "wp" + id;
            int resID = c.getResources().getIdentifier(buttonID, "id", c.getPackageName());
            chessBoard.board[6][i] = new Pawn(0, "wp", (ImageButton)rootView.findViewById(resID));
            id++;
        }

        chessBoard.whiteKingLocation[0] = 4;
        chessBoard.whiteKingLocation[1] = 7;
        chessBoard.blackKingLocation[0] = 4;
        chessBoard.blackKingLocation[1] = 0;
    }
	
	/**
	 * Prints the current contents of the board into the terminal.
	 */
	public void printBoard() {
		for (int y = 0; y <= board[0].length; y++) {
			for (int x = 0; x <= board.length; x++) {
				if (x == board.length && y != board.length) {
					System.out.print(" " + (8-y) + "\n");
				} else if (y == board.length && x != board.length) {
					System.out.print(" " + getColumnLetterFromColumnNumber(x) + " ");
				} else if (y == board.length && x == board.length) {
					System.out.print("  \n");
				} else if (board[y][x] != null) {
					System.out.print(" " + board[y][x].getStringRepresentation() + " ");
				} else if (y % 2 == 0 && x % 2 == 1){
					System.out.print(" ## ");
				} else if (y % 2 == 1 && x % 2 == 0) {
					System.out.print(" ## ");
				} else {
					System.out.print("    ");
				}
				
			}
		}
	}
	
	/**
	 * 
	 * @param x - The column number to retrieve the letter for.
	 * @return a,b,c,d,e,f,g,h for columns 0-7 respectively
	 */
	private String getColumnLetterFromColumnNumber(int x) {
		if (x == 0) {
			return " a";
		} else if (x == 1) {
			return " b";
		} else if (x == 2) {
			return " c";
		} else if (x == 3) {
			return " d";
		} else if (x == 4) {
			return " e";
		} else if (x == 5) {
			return " f";
		} else if (x == 6) {
			return " g";
		} else if (x == 7) {
			return " h";
		}
		return null;
	}
	
	/**
	 * Parses the players move from a string.
	 * Omg im passing so many variables to this.
	 */
	public boolean parseMove(int startRow, int startColumn, final int endRow, final int endColumn, int color, Context c, boolean recording, int promotion) {

		Piece[][] boardcopy = copyBoard(board);
		
		//makes a copy of the board and attempts to make the move. If it fails, fail the original move. If it succeeds test to see if own king is placed into check.
		//If own king is not put into check perform the move on the real board and return true.
		if (boardcopy[startRow][startColumn].move(startColumn, startRow, endColumn, endRow, boardcopy, c, false)) {
			if (!(board[startRow][startColumn] instanceof King) && checkIfKingIsInCheck(color, boardcopy, true)) {
				return false; //invalid move, own king is put into check
			} else {
				if (board[startRow][startColumn] instanceof King) {
					if (checkIfKingIsInCheck(color, boardcopy, endColumn, endRow)) { //Not redundant!  endColumn/endRow updated in this check.
						return false; //after the king has moved it is in check. Invalid move.
					}
					if (color == 0) {
						whiteKingLocation[0] = endColumn;
						whiteKingLocation[1] = endRow;
					} else {
						blackKingLocation[0] = endColumn;
						blackKingLocation[1] = endRow;
					}
				}
				//pawn promotion conditionals.
				if (board[startRow][startColumn] instanceof Pawn) {
					if (color == 0 && endRow == 0) {
						board[startRow][startColumn].move(startColumn, startRow, endColumn, endRow, board, c, true);
						if (recording) {
							Piece p = board[endRow][endColumn];
							ImageButton ib = p.pieceButton;
							if (promotion == 0) {
								ib.setImageResource(R.drawable.wq);
								p = new Queen(p.getColor(), "wQ", ib);
							} else if (promotion == 1) {
								ib.setImageResource(R.drawable.wr);
								p = new Rook(p.getColor(), "wR", ib);
							} else if (promotion == 2) {
								ib.setImageResource(R.drawable.wb);
								p = new Bishop(p.getColor(), "wB", ib);
							} else if (promotion == 3) {
								ib.setImageResource(R.drawable.wn);
								p = new Knight(p.getColor(), "wN", ib);
							}
							board[endRow][endColumn] = p;
						} else {
							new AlertDialog.Builder(ca)
									.setTitle(R.string.pick_piece)
									.setItems(R.array.promotable_pieces, new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											Piece p = board[endRow][endColumn];
											ImageButton ib = p.pieceButton;
											if (which == 0) {
												ib.setImageResource(R.drawable.wq);
												p = new Queen(p.getColor(), "wQ", ib);
											} else if (which == 1) {
												ib.setImageResource(R.drawable.wr);
												p = new Rook(p.getColor(), "wR", ib);
											} else if (which == 2) {
												ib.setImageResource(R.drawable.wb);
												p = new Bishop(p.getColor(), "wB", ib);
											} else if (which == 3) {
												ib.setImageResource(R.drawable.wn);
												p = new Knight(p.getColor(), "wN", ib);
											}
											board[endRow][endColumn] = p;
										}
									})
									.setCancelable(false)
									.setIcon(R.drawable.logo)
									.show();
						}
						return true;
					} 
					else if (color == 1 && endRow == 7) {
						board[startRow][startColumn].move(startColumn, startRow, endColumn, endRow, board, c, true);
						if (recording) {
							Piece p = board[endRow][endColumn];
							ImageButton ib = p.pieceButton;
							if (promotion == 0) {
								ib.setImageResource(R.drawable.bq);
								p = new Queen(p.getColor(), "bQ", ib);
							} else if (promotion == 1) {
								ib.setImageResource(R.drawable.br);
								p = new Rook(p.getColor(), "bR", ib);
							} else if (promotion == 2) {
								ib.setImageResource(R.drawable.bb);
								p = new Bishop(p.getColor(), "bB", ib);
							} else if (promotion == 3) {
								ib.setImageResource(R.drawable.bn);
								p = new Knight(p.getColor(), "bN", ib);
							}
							board[endRow][endColumn] = p;
						} else {
							new AlertDialog.Builder(ca)
									.setTitle(R.string.pick_piece)
									.setItems(R.array.promotable_pieces, new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											Piece p = board[endRow][endColumn];
											ImageButton ib = p.pieceButton;
											if (which == 0) {
												ib.setImageResource(R.drawable.bq);
												p = new Queen(p.getColor(), "bQ", ib);
											} else if (which == 1) {
												ib.setImageResource(R.drawable.br);
												p = new Rook(p.getColor(), "bR", ib);
											} else if (which == 2) {
												ib.setImageResource(R.drawable.bb);
												p = new Bishop(p.getColor(), "bB", ib);
											} else if (which == 3) {
												ib.setImageResource(R.drawable.bn);
												p = new Knight(p.getColor(), "bN", ib);
											}
											board[endRow][endColumn] = p;
										}
									})
									.setCancelable(false)
									.setIcon(R.drawable.logo)
									.show();
							return true;
						}
					}
				}
				//end pawn promotion
				return board[startRow][startColumn].move(startColumn, startRow, endColumn, endRow, board, c, true); //returns true, the move was perfectly okay!
			}
		}
		
		return false; //invalid move
		
	}
	
	/**
	 * Helper function, converts letters to numbers
	 * @param c
	 * @return integer representation of letter
	 */
	private int getColumnNumberFromColumnLetter(char c) {
		if (c == 'a') {
			return 0;
		} else if (c == 'b') {
			return 1;
		} else if (c == 'c') {
			return 2;
		} else if (c == 'd') {
			return 3;
		} else if (c == 'e') {
			return 4;
		} else if (c == 'f') {
			return 5;
		} else if (c == 'g') {
			return 6;
		} else if (c == 'h') {
			return 7;
		}
		return -1;
	}
	
	/**
	 * Starting function for checking if the king is in check. Passes the kings row and column to the main checkIfKingIsInCheck() function.
	 * You can also check if the checking piece is in the "check" which is used for checking for checkmate. If the checking piece is also in 
	 * check that means it can be destroyed and the king is not in checkmate. I hope that made sense.
	 * @param color - color of the side who's king we're checking if is in check.
	 * @param board - a 2-dimensional array of the board we're checking for.
	 * @param checkingForKing - true if we're checking if a king is in check, false if we're checking for the checking piece.
	 * @return true/false based on whether or not king or checking piece is in check.
	 */
	public boolean checkIfKingIsInCheck(int color, Piece[][] board, boolean checkingForKing) {
		if (checkingForKing) {
			int kingColumn;
			int kingRow;
			if (color == 0) {
				kingColumn = whiteKingLocation[0];
				kingRow = whiteKingLocation[1];
			} else {
				kingColumn = blackKingLocation[0];
				kingRow = blackKingLocation[1];
			}
			return checkIfKingIsInCheck(color, board, kingColumn, kingRow);
		} else {
			return checkIfKingIsInCheck(color, board, checkingPieceLocation[0], checkingPieceLocation[1]);
		}
	}
	
	
	/**
	 * Checks if king of given color is in check.
	 * @param color - color of the side who's king we're checking if is in check.
	 * @param board - a 2-dimensional array of the board we're checking for.
	 * @param kingColumn - the column the king is located in the board array.
	 * @param kingRow - the row the king is located in the board array.
	 * @return - true/false whether the king is in check or not.
	 */
	public boolean checkIfKingIsInCheck(int color, Piece [][] board, int kingColumn, int kingRow) {
		
		//check for attacking rooks or queens from the south
		for (int i = kingRow+1; i < board.length; i++) {
			if (!(board[i][kingColumn] instanceof Blank)) {
				if (board[i][kingColumn].getColor() != color && (board[i][kingColumn] instanceof Rook || board[i][kingColumn] instanceof Queen)) {
					return true;
				} else {
					break;
				}
			}
		}
		
		//check for attacking rooks or queens from the north ("The north remembers" -Ned Stark)
		for (int i = kingRow-1; i >= 0; i--) {
			if (!(board[i][kingColumn] instanceof Blank)) {
				if (board[i][kingColumn].getColor() != color && (board[i][kingColumn] instanceof Rook || board[i][kingColumn] instanceof Queen)) {
					return true;
				} else {
					break;
				}
			}
		}
		
		//check for attacking rooks or queens from the east
		for (int i = kingColumn+1; i < board.length; i++) {
			if (!(board[kingRow][i] instanceof Blank)) {
				if (board[kingRow][i].getColor() != color && (board[kingRow][i] instanceof Rook || board[kingRow][i] instanceof Queen)) {
					return true;
				} else {
					break;
				}
			}
		}
		
		//check for attacking rooks or queens from the west
		for (int i = kingColumn-1; i >= 0; i--) {
			if (!(board[kingRow][i] instanceof Blank)) {
				if (board[kingRow][i].getColor() != color && (board[kingRow][i] instanceof Rook || board[kingRow][i] instanceof Queen)) {
					return true;
				} else {
					break;
				}
			}
		}
		
		//check for knights of the old republic III, all 8 possible variations of where a knight can be placed in relation to a king.
		if (kingRow + 1 < 8 && kingColumn + 2 < 8 && board[kingRow+1][kingColumn+2] != null && board[kingRow+1][kingColumn+2].getColor() != color && board[kingRow+1][kingColumn+2] instanceof Knight) {
			return true;
		}
		
		if (kingRow + 1 < 8 && kingColumn - 2 >= 0 && board[kingRow+1][kingColumn-2] != null && board[kingRow+1][kingColumn-2].getColor() != color && board[kingRow+1][kingColumn-2] instanceof Knight) {
			return true;
		}
		
		if (kingRow + 2 < 8 && kingColumn + 1 < 8 && board[kingRow+2][kingColumn+1] != null && board[kingRow+2][kingColumn+1].getColor() != color && board[kingRow+2][kingColumn+1] instanceof Knight) {
			return true;
		}
		
		if (kingRow + 2 < 8 && kingColumn - 1 >= 0 && board[kingRow+2][kingColumn-1] != null && board[kingRow+2][kingColumn-1].getColor() != color && board[kingRow+2][kingColumn-1] instanceof Knight) {
			return true;
		}
		
		if (kingRow - 1 >= 0 && kingColumn + 2 < 8 && board[kingRow-1][kingColumn+2] != null && board[kingRow-1][kingColumn+2].getColor() != color && board[kingRow-1][kingColumn+2] instanceof Knight) {
			return true;
		}
		
		if (kingRow - 1 >= 0 && kingColumn - 2 >= 0 && board[kingRow-1][kingColumn-2] != null && board[kingRow-1][kingColumn-2].getColor() != color && board[kingRow-1][kingColumn-2] instanceof Knight) {
			return true;
		}
		
		if (kingRow - 2 >= 0 && kingColumn + 1 < 8 && board[kingRow-2][kingColumn+1] != null && board[kingRow-2][kingColumn+1].getColor() != color && board[kingRow-2][kingColumn+1] instanceof Knight) {
			return true;
		}
		
		if (kingRow - 2 >= 0 && kingColumn - 1 >= 0 && board[kingRow-2][kingColumn-1] != null && board[kingRow-2][kingColumn-1].getColor() != color && board[kingRow-2][kingColumn-1] instanceof Knight) {
			return true;
		}
		//end knight checks
		
		//check for bishops and queens attacking from southeast
		for (int i = kingRow+1, j = kingColumn+1; i < board.length && j < board.length; i++, j++) {
			if (!(board[i][j] instanceof Blank)) {
				if (board[i][j].getColor() != color && (board[i][j] instanceof Bishop || board[i][j] instanceof Queen)) {
					return true;
				} else {
					break;
				}
			}
		}
		
		//check for bishops and queens attacking from southwest
		for (int i = kingRow+1, j = kingColumn-1; i < board.length && j >= 0; i++, j--) {
			if (!(board[i][j] instanceof Blank)) {
				if (board[i][j].getColor() != color && (board[i][j] instanceof Bishop || board[i][j] instanceof Queen)) {
					return true;
				} else {
					break;
				}
			}
		}
		
		//check for bishops and queens attacking from northeast
		for (int i = kingRow-1, j = kingColumn+1; i >= 0 && j < board.length; i--, j++) {
			if (!(board[i][j] instanceof Blank)) {
				if (board[i][j].getColor() != color && (board[i][j] instanceof Bishop || board[i][j] instanceof Queen)) {
					return true;
				} else {
					break;
				}
			}
		}
		
		
		//check for bishops and queens attacking from northwest
		for (int i = kingRow-1, j = kingColumn-1; i >= 0 && j >= 0; i--, j--) {
			if (!(board[i][j] instanceof Blank)) {
				if (board[i][j].getColor() != color && (board[i][j] instanceof Bishop || board[i][j] instanceof Queen)) {
					return true;
				} else {
					break;
				}
			}
		}
		
		//Check for attacking pawns (if king is white and pawns are black) (northwest) (also known as the lamest way to get put into check)
		if (color == 0 && kingRow-1 >= 0 && kingColumn-1 >= 0 && board[kingRow-1][kingColumn-1] != null && board[kingRow-1][kingColumn-1].getColor() != color && board[kingRow-1][kingColumn-1] instanceof Pawn) {
			return true;
		}
		
		//Check for attacking pawns (if king is white and pawns are black) (northeast)
		if (color == 0 && kingRow-1 >= 0 && kingColumn+1 < 8 && board[kingRow-1][kingColumn+1] != null && board[kingRow-1][kingColumn+1].getColor() != color && board[kingRow-1][kingColumn+1] instanceof Pawn) {
			return true;
		}
		
		//Check for attacking pawns (if king is black and pawns are white) (southwest)
		if (color == 1 && kingRow+1 < 8 && kingColumn-1 >= 0 && board[kingRow+1][kingColumn-1] != null && board[kingRow+1][kingColumn-1].getColor() != color && board[kingRow+1][kingColumn-1] instanceof Pawn) {
			return true;
		}
		
		//Check for attacking pawns (if king is black and pawns are white) (southeast)
		if (color == 1 && kingRow+1 < 8 && kingColumn+1 < 8 && board[kingRow+1][kingColumn+1] != null && board[kingRow+1][kingColumn+1].getColor() != color && board[kingRow+1][kingColumn+1] instanceof Pawn) {
			return true;
		}
		
		//Check for attacking king (NOTE: In normal play this cannot happen. A king cannot put another king in check without putting itself in check.
		//However this is useful for preemptively checking if moving your king will put it in check with another king. (I.E. illegal move)).
		if (kingRow - 1 >= 0 && kingColumn-1 >= 0 && board[kingRow-1][kingColumn-1] != null && board[kingRow-1][kingColumn-1].getColor() != color && board[kingRow-1][kingColumn-1] instanceof King) {
			return true;
		}
		
		if (kingRow - 1 >= 0 && board[kingRow-1][kingColumn] != null && board[kingRow-1][kingColumn].getColor() != color && board[kingRow-1][kingColumn] instanceof King) {
			return true;
		}
		
		if (kingRow - 1 >= 0 && kingColumn+1 < 8 && board[kingRow-1][kingColumn+1] != null && board[kingRow-1][kingColumn+1].getColor() != color && board[kingRow-1][kingColumn+1] instanceof King) {
			return true;
		}
		
		if (kingColumn-1 >= 0 && board[kingRow][kingColumn-1] != null && board[kingRow][kingColumn-1].getColor() != color && board[kingRow][kingColumn-1] instanceof King) {
			return true;
		}
		
		if (kingColumn+1 < 8 && board[kingRow][kingColumn+1] != null && board[kingRow][kingColumn+1].getColor() != color && board[kingRow][kingColumn+1] instanceof King) {
			return true;
		}

		if (kingRow + 1 < 8 && kingColumn-1 >= 0 && board[kingRow+1][kingColumn-1] != null && board[kingRow+1][kingColumn-1].getColor() != color && board[kingRow+1][kingColumn-1] instanceof King) {
			return true;
		}
		
		if (kingRow + 1 < 8 && board[kingRow+1][kingColumn] != null && board[kingRow+1][kingColumn].getColor() != color && board[kingRow+1][kingColumn] instanceof King) {
			return true;
		}
		
		if (kingRow + 1 < 8 && kingColumn+1 < 8 && board[kingRow+1][kingColumn+1] != null && board[kingRow+1][kingColumn+1].getColor() != color && board[kingRow+1][kingColumn+1] instanceof King) {
			return true;
		}
		//end king checks
		
		
		return false;
	}
	
	/**
	 * Checks if checkmate has occurred.
	 * @param color - color of side who's king is in check.
	 * @return true/false whether if checkmate has occurred.
	 */
	public boolean checkForCheckmate(int color) {
		
		if (canTheKingMakeAValidMove(color)) {
			return false; //
		}
		
		int enemyColor;
		if (color == 0) {
			enemyColor = 1;
		} else {
			enemyColor = 0;
		}
		
		if (checkIfKingIsInCheck(enemyColor, board, false)) {
			return false; //if checking piece is in "check" it can be destroyed by another piece.
		}
		
		return true;
	}
	
	/**
	 * Checks if stalemate has occurred.
	 * @param color - the color that stalemate is being checked for
	 * @return true if a stalemate has occurred, false otherwise.
	 */
	public boolean checkForStalemate(int color) {
		
		if (canTheKingMakeAValidMove(color)) {
			return false;
		}
		
		Piece[][] boardcopy = copyBoard(board);
		
		//attempt to make a move with everyone single one of the the colors piece.
		for (int i = 0; i < boardcopy.length; i++) {
			for (int j = 0; j < boardcopy[0].length; j++) {
				if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Pawn) {
					if (color == 0) {
						if (i != 0 && attemptMove(j, i, j, i-1, color)) {
							return false;
						}
						if (i != 0 && j != 0 && attemptMove(j, i, j-1, i-1, color)) {
							return false;
						}
						if (i != 0 && j != 7 && attemptMove(j, i, j+1, i-1, color)) {
							return false;
						}
					} else {
						if (i != 7 && attemptMove(j, i, j, i+1, color)) {
							return false;
						}
						if (i != 7 && j != 0 && attemptMove(j, i, j-1, i+1, color)) {
							return false;
						}
						if (i != 0 && j != 7 && attemptMove(j, i, j+1, i+1, color)) {
							return false;
						}
					}
				}
				else if (boardcopy[i][j] != null &&  boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Rook) {
					if (i != 0 && attemptMove(j, i, j, i-1, color)) {
						return false;
					}
					if (i != 7 && attemptMove(j, i, j, i+1, color)) {
						return false;
					}
					if (j != 0 && attemptMove(j, i, j-1, i, color)) {
						return false;
					}
					if (j != 7 && attemptMove(j, i, j+1, i, color)) {
						return false;
					}
				}
				else if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Bishop) {
					if (i != 0 && j != 7  && attemptMove(j, i, j+1, i-1, color)) {
						return false;
					}
					if (i != 0 && j != 0  && attemptMove(j, i, j-1, i-1, color)) {
						return false;
					}
					if (i != 7 && j != 0  && attemptMove(j, i, j-1, i+1, color)) {
						return false;
					}
					if (i != 7 && j != 7  && attemptMove(j, i, j+1, i+1, color)) {
						return false;
					}	
				}
				else if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Queen) {
					if (i != 0 && attemptMove(j, i, j, i-1, color)) {
						return false;
					}
					if (i != 7 && attemptMove(j, i, j, i+1, color)) {
						return false;
					}
					if (j != 0 && attemptMove(j, i, j-1, i, color)) {
						return false;
					}
					if (j != 7 && attemptMove(j, i, j+1, i, color)) {
						return false;
					}
					if (i != 0 && j != 7  && attemptMove(j, i, j+1, i-1, color)) {
						return false;
					}
					if (i != 0 && j != 0  && attemptMove(j, i, j-1, i-1, color)) {
						return false;
					}
					if (i != 7 && j != 0  && attemptMove(j, i, j-1, i+1, color)) {
						return false;
					}
					if (i != 7 && j != 7  && attemptMove(j, i, j+1, i+1, color)) {
						return false;
					}	
				}
				else if (boardcopy[i][j] != null && boardcopy[i][j].getColor() == color && boardcopy[i][j] instanceof Knight) {
					if (i + 1 < 8 && j + 2 < 8 && attemptMove(j, i, j+2, i+1, color)) {
						return false;
					}
					
					if (i + 1 < 8 && j - 2 >= 0 && attemptMove(j, i, j-2, i+1, color)) {
						return false;
					}
					
					if (i + 2 < 8 && j + 1 < 8 && attemptMove(j, i, j+1, i+2, color)) {
						return false;
					}
					
					if (i + 2 < 8 && j - 1 >= 0 && attemptMove(j, i, j-1, i+2, color)) {
						return false;
					}
					
					if (i - 1 >= 0 && j + 2 < 8 && attemptMove(j, i, j+2, i-1, color)) {
						return false;
					}
					
					if (i - 1 >= 0 && j - 2 >= 0 && attemptMove(j, i, j-2, i-1, color)) {
						return false;
					}
					
					if (i - 2 >= 0 && j + 1 < 8 && attemptMove(j, i, j+1, i-2, color)) {
						return false;
					}
					
					if (i - 2 >= 0 && j - 1 >= 0 && attemptMove(j, i, j-1, i-2, color)) {
						return false;
					}
				}
			}
		}
		
		return true;
	}
	
	
	/**
	 * Checks if the can king move to escape check.
	 * Can be also used for the beginning stages of checking for stalemate.
	 * @param color - color of king that is being checked for valid moves.
	 * @return true/false depending if the king can move or not.
	 */
	public boolean canTheKingMakeAValidMove(int color) {
		
		int kingColumn;
		int kingRow;
		if (color == 0) {
			kingColumn = whiteKingLocation[0];
			kingRow = whiteKingLocation[1];
		} else {
			kingColumn = blackKingLocation[0];
			kingRow = blackKingLocation[1];
		}
		
		Piece[][] boardcopy = copyBoard(board);
		
		//Attempting to move the king in all directions.
		if (kingRow - 1 >= 0 && kingColumn-1 >= 0 && attemptMove(kingColumn, kingRow, kingColumn-1, kingRow-1, color)) {
			return true;
		}
		
		if (kingRow - 1 >= 0 && attemptMove(kingColumn, kingRow, kingColumn, kingRow-1, color)) {
			return true;
		}
		
		if (kingRow - 1 >= 0 && kingColumn+1 < 8 && attemptMove(kingColumn, kingRow, kingColumn+1, kingRow-1, color)) {
			return true;
		}
		
		if (kingColumn-1 >= 0 && attemptMove(kingColumn, kingRow, kingColumn-1, kingRow, color)) {
			return true;
		}
		
		if (kingColumn+1 < 8 && attemptMove(kingColumn, kingRow, kingColumn+1, kingRow, color)) {
			return true;
		}

		if (kingRow + 1 < 8 && kingColumn-1 >= 0 && attemptMove(kingColumn, kingRow, kingColumn-1, kingRow+1, color)) {
			return true;
		}
		
		if (kingRow + 1 < 8 && attemptMove(kingColumn, kingRow, kingColumn, kingRow+1, color)) {
			return true;
		}
		
		if (kingRow + 1 < 8 && kingColumn+1 < 8 && attemptMove(kingColumn, kingRow, kingColumn+1, kingRow+1, color)) {
			return true;
		}
		
		
		return false; //King cannot move in any direction.
	}
	
	/**
	 * Checks to see if move is valid. Can be abstracted later for all moves for all pieces but currently only valid for checking if king can make a valid move.
	 * @param startColumn - starting column of the piece
	 * @param startRow - starting row of the piece
	 * @param endColumn - destination column of the piece
	 * @param endRow - destination row of the piece
	 * @param color - color of the piece
	 * @return - true if the move can be made. false otherwise.
	 */
	public boolean attemptMove(int startColumn, int startRow, int endColumn, int endRow, int color) {
		Piece[][] boardcopy = copyBoard(board);
		
		//This only checks for moving the king.
		if (boardcopy[startRow][startColumn] instanceof King) {
			if (boardcopy[startRow][startColumn].move(startColumn, startRow, endColumn, endRow, boardcopy, ca, false) && !checkIfKingIsInCheck(color, boardcopy, endColumn, endRow)) {
				return true;	
			}
		}
		//If we're trying to move a piece besides the king.
		else {
			if (boardcopy[startRow][startColumn].move(startColumn, startRow, endColumn, endRow, boardcopy, ca, false) && !checkIfKingIsInCheck(color, boardcopy, true)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Creates a copy of the board and its pieces at its current state.
	 * @param board - board to be copied
	 * @return a copy of the board.
	 */
	public Piece[][] copyBoard(Piece [][] board) {
		Piece[][] boardcopy = new Piece[8][8];
		
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] instanceof Rook) {
					boardcopy[i][j] = new Rook(board[i][j].getColor(), board[i][j].getStringRepresentation(), null);
				}
				else if (board[i][j] instanceof King) {
					boardcopy[i][j] = new King(board[i][j].getColor(), board[i][j].getStringRepresentation(), null);
				} else {
					boardcopy[i][j] = board[i][j];
				}
			}
		}
		
		return boardcopy;
	}
}

	
