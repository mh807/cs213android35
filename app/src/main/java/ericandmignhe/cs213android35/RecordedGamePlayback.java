package ericandmignhe.cs213android35;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ericandmignhe.cs213android35.board.Board;
import ericandmignhe.cs213android35.piece.Blank;

public class RecordedGamePlayback extends Activity {

    Board chessBoard;

    TextView turnIdicator;
    TextView checkStatus;

    private ArrayList<int[]> moves;

    private int currentMove = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorded_game_playback);

        moves = Recording.recordingsList.get(this.getIntent().getExtras().getInt("moves")).moveList;

        ActionBar ab = getActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        setTitle(this.getIntent().getExtras().getString("title"));

        chessBoard = new Board(this);
        chessBoard.createNewGame(chessBoard, this);

        turnIdicator = (TextView) findViewById(R.id.playerTurnIndicator);
        checkStatus = (TextView) findViewById(R.id.currentCheckStatus);

        Chess.isGameOver = false;
    }

    public void nextMove(View view) {
        if (currentMove < moves.size() && Chess.isGameOver == false) {
            if (moves.get(currentMove)[0] == 10) {
                Chess.isGameOver = true;
                hideMovableTiles();
                checkStatus.setText("Draw.");
                turnIdicator.setText("");
            }
            else if (moves.get(currentMove)[0] == 11) {
                Chess.isGameOver = true;
                hideMovableTiles();
                if (currentMove % 2 == 0) {
                    checkStatus.setText("White resigned. Black wins!");
                } else {
                    checkStatus.setText("Black resigned. White wins!");
                }
                turnIdicator.setText("");
            } else {
                chessBoard.parseMove(moves.get(currentMove)[0], moves.get(currentMove)[1],
                        moves.get(currentMove)[2], moves.get(currentMove)[3], currentMove % 2,
                        this, true, moves.get(currentMove)[4]);
                moveEffects();
                currentMove++;
            }
        } else {
            Toast.makeText(RecordedGamePlayback.this, "Game has ended. No more moves.", Toast.LENGTH_SHORT).show();
        }
    }

    public void moveEffects() {
        checkStatus.setText("");
        Chess.endWindowToPerformEnPassant();
        if (currentMove % 2 == 0) {
            if (chessBoard.checkIfKingIsInCheck(1, chessBoard.board, true)) {
                if (chessBoard.checkForCheckmate(1)) {
                    //Checkmate
                    chessBoard.endGame(0);
                    checkStatus.setText("Checkmate, white wins!");
                    turnIdicator.setText("");
                    hideMovableTiles();
                    return;
                } else {
                    //Black is in check
                    checkStatus.setText("Black is in check!");
                }
            }
            if (chessBoard.checkForStalemate(0) || chessBoard.checkForStalemate(1)) {
                //Stalemate
                Chess.isGameOver = true;
                checkStatus.setText("Stalemate.");
                turnIdicator.setText("");
                hideMovableTiles();
                return;
            }
        }
        else if (currentMove % 2 == 1) {
            if (chessBoard.checkIfKingIsInCheck(0, chessBoard.board, true)) {
                if (chessBoard.checkForCheckmate(0)) {
                    //Checkmate
                    chessBoard.endGame(1);
                    checkStatus.setText("Checkmate, black wins!");
                    turnIdicator.setText("");
                    hideMovableTiles();
                    return;
                } else {
                    //White is in check
                    checkStatus.setText("White is in check!");
                }
            }
            if (chessBoard.checkForStalemate(0) || chessBoard.checkForStalemate(1)) {
                //Stalemate
                Chess.isGameOver = true;
                checkStatus.setText("Stalemate.");
                turnIdicator.setText("");
                hideMovableTiles();
                return;
            }
        }

        if (currentMove % 2 == 0) {
            turnIdicator.setText("Black to move");
        } else {
            turnIdicator.setText("White to move");
        }
        hideMovableTiles();
    }

    public void back(View v) {
        String message = "Are you sure you want to go back to the game records menu?";

        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Chess.isGameOver = false;
                        Intent intent = new Intent(RecordedGamePlayback.this, RecordingsActivity.class);
                        RecordedGamePlayback.this.startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                })
                .setIcon(R.drawable.logo)
                .show();
    }

    public void hideMovableTiles() {
        for (int i = 0; i < chessBoard.board.length; i++) {
            for (int j = 0; j < chessBoard.board[i].length; j++) {
                if (chessBoard.board[i][j] instanceof Blank) {
                    if (i % 2 == 0) {
                        if (j % 2 == 0) {
                            chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#FBC99F"), PorterDuff.Mode.DARKEN);
                        } else {
                            chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#D18B47"), PorterDuff.Mode.DARKEN);
                        }
                    } else {
                        if (j % 2 == 0) {
                            chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#D18B47"), PorterDuff.Mode.DARKEN);
                        } else {
                            chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#FBC99F"), PorterDuff.Mode.DARKEN);
                        }
                    }
                } else {
                    chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#d9d9d9"), PorterDuff.Mode.DARKEN);
                }
            }
        }
    }



}
