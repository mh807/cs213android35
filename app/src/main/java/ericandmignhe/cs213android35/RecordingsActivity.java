package ericandmignhe.cs213android35;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toolbar;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

public class RecordingsActivity extends Activity {

    enum SortMode {NONE, TITLE, TITLE_REVERSED, DATE, DATE_REVERSED};

    SortMode currentSort = SortMode.TITLE;

    private ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_records);

        ActionBar ab = getActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        listview = (ListView) findViewById(R.id.game_list);
        sortListByDate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recording_overflow_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sortTitleOption:
                sortListByTitle();
                return true;
            case R.id.sortDateOption:
                sortListByDate();
                return true;
            case R.id.clearAllEntriesOption:
                try {
                    clearGamesFromList();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sortListByTitle() {
        if (currentSort != SortMode.TITLE) {
            Collections.sort(Recording.recordingsList, new Comparator<Recording>() {
                @Override
                public int compare(Recording rec1, Recording rec2) {
                    return rec1.name.compareTo(rec2.name);
                }
            });
            currentSort = SortMode.TITLE;
        } else if (currentSort == SortMode.TITLE) {
            Collections.sort(Recording.recordingsList, new Comparator<Recording>() {
                @Override
                public int compare(Recording rec1, Recording rec2) {
                    return rec2.name.compareTo(rec1.name);
                }
            });
            currentSort = SortMode.TITLE_REVERSED;
        }
        displayList();
    }

    public void sortListByDate() {
        if (currentSort != SortMode.DATE) {
            Collections.sort(Recording.recordingsList, new Comparator<Recording>() {
                @Override
                public int compare(Recording rec1, Recording rec2) {
                    return rec1.date.compareTo(rec2.date);
                }
            });
            currentSort = SortMode.DATE;
        } else if (currentSort == SortMode.DATE) {
            Collections.sort(Recording.recordingsList, new Comparator<Recording>() {
                @Override
                public int compare(Recording rec1, Recording rec2) {
                    return rec2.date.compareTo(rec1.date);
                }
            });
            currentSort = SortMode.DATE_REVERSED;
        }
        displayList();
    }

    public void displayList() {
        RecordingAdapter adapter = new RecordingAdapter(this, Recording.recordingsList);
        listview.setAdapter(adapter);

        final Context context = this;
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent detailIntent = new Intent(context, RecordedGamePlayback.class);

                detailIntent.putExtra("moves", position);
                detailIntent.putExtra("title", Recording.recordingsList.get(position).name);

                startActivity(detailIntent);
            }
        });

    }

    public void clearGamesFromList() throws IOException {
        Recording.recordingsList.clear();
        Recording.writeRecordings(Recording.recordingsList);
        displayList();
    }

}
