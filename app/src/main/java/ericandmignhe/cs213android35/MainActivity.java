package ericandmignhe.cs213android35;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.Toolbar;

import java.io.File;
import java.io.IOException;

import ericandmignhe.cs213android35.piece.Bishop;
import ericandmignhe.cs213android35.piece.Knight;
import ericandmignhe.cs213android35.piece.Piece;
import ericandmignhe.cs213android35.piece.Queen;
import ericandmignhe.cs213android35.piece.Rook;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Recording.storeDir = this.getFilesDir().getAbsolutePath();

        try {
            Recording.readRecordings();
            Log.i("CHESS", "Recordings successfully imported");
        } catch (IOException e) {
            Log.i("CHESS", "Recordings file not found, creating file..." + this.getFilesDir().getAbsolutePath());
            new File(this.getFilesDir(), "recordings.dat");
        } catch (ClassNotFoundException e) {
            Log.i("CHESS", "Class not found");
        }
    }

    public void createNewGame(View v) {
        Intent intent = new Intent(MainActivity.this, ChessActivity.class);
        MainActivity.this.startActivity(intent);
    }

    public void showRecordings(View v) {
        Intent intent = new Intent(MainActivity.this, RecordingsActivity.class);
        MainActivity.this.startActivity(intent);
    }
}
