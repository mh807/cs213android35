package ericandmignhe.cs213android35;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ericandmignhe.cs213android35.board.Board;
import ericandmignhe.cs213android35.piece.Bishop;
import ericandmignhe.cs213android35.piece.Blank;
import ericandmignhe.cs213android35.piece.King;
import ericandmignhe.cs213android35.piece.Knight;
import ericandmignhe.cs213android35.piece.Pawn;
import ericandmignhe.cs213android35.piece.Piece;
import ericandmignhe.cs213android35.piece.Queen;
import ericandmignhe.cs213android35.piece.Rook;

public class ChessActivity extends Activity  {

    Board chessBoard;

    /*
    Variables to track if a buttons been pressed.
     */
    int startRow = -1;
    int startCol = -1;
    ImageButton sp = null;

    boolean isPieceSelected = false;

    //0 = White, 1 = Black
    public int turn = 0;

    TextView turnIdicator;
    TextView checkStatus;

    ArrayList<int[]> gameRecord = new ArrayList<>();

    boolean previouslyUndidMove = false;
    
    Piece lastCapturedPiece = null;
    int lastCapturedPieceRow = -1;
    int lastCapturedPieceCol = -1;
    /**
     * 0 - Normal
     * 1 - Pawn Promotion
     * 2 - Castling
     * 3 - En Passant
     */
    int lastMoveType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chessBoard = new Board(this);
        chessBoard.createNewGame(chessBoard, this);

        turnIdicator = (TextView) findViewById(R.id.playerTurnIndicator);
        checkStatus = (TextView) findViewById(R.id.currentCheckStatus);

        Chess.isGameOver = false;
    }

    public void pieceSelect(View view) {
        for (int i = 0; i < chessBoard.board.length; i++) {
            for (int j = 0; j < chessBoard.board.length; j++) {
                if (view.equals(chessBoard.board[i][j].pieceButton)) {
                    if (isPieceSelected == false) {
                        if (chessBoard.board[i][j].getColor() != turn) {
                            Log.i("CHESS", "Invalid Color Selected");
                            return;
                        }
                        if (chessBoard.board[i][j].getIsPiece()) {
                            startRow = i;
                            startCol = j;
                            sp = chessBoard.board[i][j].pieceButton;
                            isPieceSelected = true;
                            showMovableTiles(i, j);
                            //Log.i("CHESS", "Clicked button - Row: " + i + " Column: " + j);
                        }
                    }
                    else if (isPieceSelected) {
                        if (startRow == i && startCol == j) {
                            Log.i("CHESS", "Two pieces are the same: " + view.equals(sp));
                            startRow = -1;
                            startCol = -1;
                            sp = null;
                            isPieceSelected = false;
                            hideMovableTiles();
                        } else if (chessBoard.board[startRow][startCol].getColor() == chessBoard.board[i][j].getColor()) {
                            hideMovableTiles();
                            startRow = i;
                            startCol = j;
                            sp = chessBoard.board[i][j].pieceButton;
                            showMovableTiles(i,j);
                        }
                        else {
                            setUndoParameters(i, j);
                            if (!chessBoard.parseMove(startRow, startCol, i, j, turn, this, false, -1)) {
                                Context context = getApplicationContext();
                                CharSequence text = "Invalid move, try again.";
                                int duration = Toast.LENGTH_SHORT;
                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                                lastCapturedPiece = null;
                                lastCapturedPieceRow = -1;
                                lastCapturedPieceCol = -1;
                            } else {
                                previouslyUndidMove = false;
                                gameRecord.add(new int[]{startRow, startCol, i, j, -1});
                                moveEffects();
                            }
                        }
                    }
                    return;
                }
            }
        }
    }

    public void moveEffects() {
        startRow = -1;
        startCol = -1;
        sp = null;
        isPieceSelected = false;
        checkStatus.setText("");
        Chess.endWindowToPerformEnPassant();
        if (turn == 0) {
            if (chessBoard.checkIfKingIsInCheck(1, chessBoard.board, true)) {
                if (chessBoard.checkForCheckmate(1)) {
                    //Checkmate
                    chessBoard.endGame(0);
                    checkStatus.setText("Checkmate, white wins!");
                    turnIdicator.setText("");
                    hideMovableTiles();
                    turn = 2;
                    showRecordGameDialog();
                    return;
                } else {
                    //Black is in check
                    checkStatus.setText("Black is in check!");
                }
            }
            if (chessBoard.checkForStalemate(0) || chessBoard.checkForStalemate(1)) {
                //Stalemate
                Chess.isGameOver = true;
                hideMovableTiles();
                checkStatus.setText("Stalemate.");
                turnIdicator.setText("");
                turn = 2;
                showRecordGameDialog();
                return;
            }
        }
        else if (turn == 1) {
            if (chessBoard.checkIfKingIsInCheck(0, chessBoard.board, true)) {
                if (chessBoard.checkForCheckmate(0)) {
                    //Checkmate
                    chessBoard.endGame(1);
                    checkStatus.setText("Checkmate, black wins!");
                    turnIdicator.setText("");
                    hideMovableTiles();
                    turn = 2;
                    showRecordGameDialog();
                    return;
                } else {
                    //White is in check
                    checkStatus.setText("White is in check!");
                }
            }
            if (chessBoard.checkForStalemate(0) || chessBoard.checkForStalemate(1)) {
                //Stalemate
                Chess.isGameOver = true;
                hideMovableTiles();
                checkStatus.setText("Stalemate.");
                turnIdicator.setText("");
                turn = 2;
                showRecordGameDialog();
                return;
            }
        }

        if (turn == 0) {
            turn = 1;
            turnIdicator.setText("Black to move");
        } else {
            turn = 0;
            turnIdicator.setText("White to move");
        }
        hideMovableTiles();
    }

    public void setUndoParameters(int endRow, int endCol) {
        lastCapturedPiece = chessBoard.board[endRow][endCol];
        lastCapturedPieceRow = endRow;
        lastCapturedPieceCol = endCol;
        lastMoveType = 0;
        if (chessBoard.board[startRow][startCol] instanceof Pawn) {
            if (turn == 0) {
                if (endRow == 0) {
                    lastMoveType = 1;
                } else if (endRow - startRow == -1 && Math.abs(startCol - endCol) == 1 && 
                        chessBoard.board[endRow][endCol] instanceof Blank && 
                        chessBoard.board[endRow+1][endCol] != null && 
                        chessBoard.board[endRow+1][endCol].getColor() != turn && 
                        chessBoard.board[endRow+1][endCol] == (Piece)Chess.recentlyDoubleMovedPawnQueue.peek()) {
                    lastMoveType = 3;
                }
            } else if (turn == 1) {
                if (endRow == 7) {
                    lastMoveType = 1;
                } else if (endRow - startRow == 1 && Math.abs(startCol - endCol) == 1 
                        && chessBoard.board[endRow][endCol] instanceof Blank
                        && chessBoard.board[endRow-1][endCol] != null && 
                        chessBoard.board[endRow-1][endCol].getColor() != turn &&
                        chessBoard.board[endRow-1][endCol] == (Piece)Chess.recentlyDoubleMovedPawnQueue.peek()) {
                    lastMoveType = 3;
                }
            }
        }
        else if (chessBoard.board[startRow][startCol] instanceof King) {
            if (!chessBoard.board[startRow][startCol].hasMoved) {
                if (startRow == endRow && endCol == startCol + 2 && chessBoard.board[startRow][startCol+1] instanceof Blank && chessBoard.board[startRow][startCol+2] instanceof Blank
                        && chessBoard.board[startRow][startCol+3] instanceof Rook && chessBoard.board[startRow][startCol+3].hasMoved == false) {
                    lastMoveType = 2;
                } else if (startRow == endRow && endCol == startCol - 2 && chessBoard.board[startRow][startCol-1] instanceof Blank && chessBoard.board[startRow][startCol-2] instanceof Blank
                        && chessBoard.board[startRow][startCol-3] instanceof Blank && chessBoard.board[startRow][startCol-4] instanceof Rook
                        && chessBoard.board[startRow][startCol-4].hasMoved == false) {
                    lastMoveType = 2;
                }
            }
        }
    }
    
    public void undo(View view) {
        if (Chess.isGameOver) {
            showToast("Action unavailable, the game has already ended.");
            return;
        } else if (gameRecord.size() == 0 || previouslyUndidMove) {
            showToast("Action unavailable, no moves to undo.");
            return;
        } else {
            int[] lastMove = gameRecord.get(gameRecord.size()-1);

            int endRow = lastMove[0];
            int endColumn = lastMove[1];
            int startRow = lastMove[2];
            int startColumn = lastMove[3];

            if (lastMoveType == 1) {
                ImageButton temp = chessBoard.board[startRow][startColumn].pieceButton;
                chessBoard.board[startRow][startColumn] = new Pawn(((turn+1) % 2), "WHO CARES", temp);
            } else if (lastMoveType == 2) {
                if ((startColumn - endColumn) < 0) {
                    chessBoard.board[startRow][startColumn+1].hasMoved = false;
                    chessBoard.board[startRow][startColumn].hasMoved = false;
                    ImageButton temp = chessBoard.board[startRow][startColumn+1].pieceButton;
                    chessBoard.board[startRow][startColumn+1].pieceButton = chessBoard.board[endRow][endColumn-4].pieceButton;
                    assignImage(chessBoard.board[startRow][startColumn+1], turn);
                    Piece previousTile = new Blank(2, "WHO CARES", temp);
                    temp.setImageResource(0);
                    assignImage(previousTile, ((turn+1) % 2));

                    chessBoard.board[endRow][endColumn-4] = chessBoard.board[startRow][startColumn+1];
                    chessBoard.board[startRow][startColumn+1] = previousTile;
                } else if ((startColumn - endColumn) > 0) {
                    chessBoard.board[startRow][startColumn-1].hasMoved = false;
                    chessBoard.board[startRow][startColumn].hasMoved = false;
                    ImageButton temp = chessBoard.board[startRow][startColumn-1].pieceButton;
                    chessBoard.board[startRow][startColumn-1].pieceButton = chessBoard.board[endRow][endColumn+3].pieceButton;
                    assignImage(chessBoard.board[startRow][startColumn-1], turn);
                    Piece previousTile = new Blank(2, "WHO CARES", temp);
                    temp.setImageResource(0);
                    assignImage(previousTile, ((turn+1) % 2));

                    chessBoard.board[endRow][endColumn+3] = chessBoard.board[startRow][startColumn-1];
                    chessBoard.board[startRow][startColumn-1] = previousTile;
                }
            } else if(lastMoveType == 3) {
                if (turn == 0) {
                    ImageButton temp = chessBoard.board[startRow-1][startColumn].pieceButton;
                    chessBoard.board[startRow-1][startColumn] = new Pawn(turn, "WHO CARES", temp);
                    assignImage(chessBoard.board[startRow-1][startColumn], ((turn+1) % 2));
                    Chess.recentlyDoubleMovedPawnQueue.add((Pawn)chessBoard.board[startRow-1][startColumn]);
                } else if (turn == 1) {
                    ImageButton temp = chessBoard.board[startRow+1][startColumn].pieceButton;
                    chessBoard.board[startRow+1][startColumn] = new Pawn(turn, "WHO CARES", temp);
                    assignImage(chessBoard.board[startRow+1][startColumn], ((turn+1) % 2));
                    Chess.recentlyDoubleMovedPawnQueue.add((Pawn)chessBoard.board[startRow+1][startColumn]);
                }
            }

            ImageButton temp = chessBoard.board[startRow][startColumn].pieceButton;
            chessBoard.board[startRow][startColumn].pieceButton = chessBoard.board[endRow][endColumn].pieceButton;
            assignImage(chessBoard.board[startRow][startColumn], turn);
            Piece previousTile = lastCapturedPiece;
            temp.setImageResource(0);
            previousTile.pieceButton = temp;
            assignImage(previousTile, ((turn+1) % 2));

            chessBoard.board[endRow][endColumn] = chessBoard.board[startRow][startColumn];
            chessBoard.board[startRow][startColumn] = previousTile;

            previouslyUndidMove = true;
            gameRecord.remove(gameRecord.size()-1);
            moveEffects();
        }
    }
    
    public void assignImage(Piece p, int color) {
        if (p instanceof Pawn) {
            if (color == 1) {
                p.pieceButton.setImageResource(R.drawable.wp);
            } else {
                p.pieceButton.setImageResource(R.drawable.bp);
            }
        }
        else if (p instanceof Rook) {
            if (color == 1) {
                p.pieceButton.setImageResource(R.drawable.wr);
            } else {
                p.pieceButton.setImageResource(R.drawable.br);
            }
        }
        else if (p instanceof Knight) {
            if (color == 1) {
                p.pieceButton.setImageResource(R.drawable.wn);
            } else {
                p.pieceButton.setImageResource(R.drawable.bn);
            }
        }
        else if (p instanceof Bishop) {
            if (color == 1) {
                p.pieceButton.setImageResource(R.drawable.wb);
            } else {
                p.pieceButton.setImageResource(R.drawable.bb);
            }
        }
        else if (p instanceof Queen) {
            if (color == 1) {
                p.pieceButton.setImageResource(R.drawable.wq);
            } else {
                p.pieceButton.setImageResource(R.drawable.bq);
            }
        }
        else if (p instanceof King) {
            if (color == 1) {
                p.pieceButton.setImageResource(R.drawable.wk);
            } else {
                p.pieceButton.setImageResource(R.drawable.bk);
            }
        }
    }

    public void ai(View view) {
        if (Chess.isGameOver) {
            showToast("Action unavailable, the game has already ended.");
            return;
        }
        startRow = -1;
        startCol = -1;
        sp = null;
        isPieceSelected = false;
        hideMovableTiles();
        AI ai = new AI();
        if (turn == 0) {
            int [] move = ai.findFirstAvailableMove(0, chessBoard.board, this, chessBoard.whiteKingLocation, chessBoard.blackKingLocation);
            startRow = move[1];
            startCol = move[0];
            if (move == null) {
                showToast("No move found!");
            } else {
                setUndoParameters(move[3], move[2]);
                if (!chessBoard.parseMove(move[1], move[0], move[3], move[2], turn, this, false, -1)) {
                    showToast("AI found a bad move.");
                } else {
                    gameRecord.add(new int[]{move[1], move[0], move[3], move[2], -1});
                    moveEffects();
                }
            }
        } else {
            int [] move = ai.findFirstAvailableMove(1, chessBoard.board, this, chessBoard.whiteKingLocation, chessBoard.blackKingLocation);
            startRow = move[1];
            startCol = move[0];
            if (move == null) {
                showToast("No move found!");
            } else {
                setUndoParameters(move[3], move[2]);
                if (!chessBoard.parseMove(move[1], move[0], move[3], move[2], turn, this, false, -1)) {
                    showToast("AI found a bad move.");
                } else {
                    gameRecord.add(new int[]{move[1], move[0], move[3], move[2], -1});
                    moveEffects();
                }
            }
        }
        startRow = -1;
        startCol = -1;
    }

    public void showRecordGameDialog() {
        LayoutInflater infl = this.getLayoutInflater();
        final View inflater = infl.inflate(R.layout.save_recording, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setView(inflater);

        final EditText gameNameTextField = (EditText) inflater.findViewById(R.id.gamename);

        builder.setView(inflater)
                .setTitle("Would you like to save a recording of your game?")
                // Add action buttons
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //Do nothing. This gets overridden later.
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Do nothing, dismiss the dialog.
                    }
                })
                .setCancelable(false);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String gameName = gameNameTextField.getText().toString();

                if (gameName.equals("")) {
                    Toast.makeText(ChessActivity.this, "Game name is a required field.", Toast.LENGTH_SHORT).show();
                } else {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String formattedDate = df.format(c.getTime());
                    Recording r = new Recording(gameName, formattedDate, gameRecord);
                    Recording.recordingsList.add(r);
                    try {
                        Recording.writeRecordings(Recording.recordingsList);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
            }
        });

    }

    public void draw(View v) {
        if (Chess.isGameOver) {
            showToast("Action unavailable, the game has already ended.");
        } else {
            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to end the game in a draw?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            gameRecord.add(new int[]{10, -1, -1, -1, -1});
                            Chess.isGameOver = true;
                            hideMovableTiles();
                            checkStatus.setText("Draw.");
                            turnIdicator.setText("");
                            turn = 2;
                            showRecordGameDialog();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    })
                    .setIcon(R.drawable.logo)
                    .show();
        }
    }

    public void resign(View v) {
        if (Chess.isGameOver) {
            showToast("Action unavailable, the game has already ended.");
        } else {
            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to resign?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            gameRecord.add(new int[]{11, -1, -1, -1, -1});
                            Chess.isGameOver = true;
                            hideMovableTiles();
                            if (turn == 0) {
                                checkStatus.setText("White resigned. Black wins!");
                            } else {
                                checkStatus.setText("Black resigned. White wins!");
                            }
                            turnIdicator.setText("");
                            turn = 2;
                            showRecordGameDialog();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    })
                    .setIcon(R.drawable.logo)
                    .show();
        }
    }

    public void newGame(View v) {
        String message = "";
        if (Chess.isGameOver) {
            message = "Are you sure you want to go start a new game?";
        } else {
            message = "All current game progress will be lost. Do you still wish to start a new game?";
        }
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(ChessActivity.this, ChessActivity.class);
                        ChessActivity.this.startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                })
                .setIcon(R.drawable.logo)
                .show();
    }

    public void back(View v) {
        String message = "";
        if (Chess.isGameOver) {
            message = "Are you sure you want to go back to the main menu?";
        } else {
            message = "All current game progress will be lost. Do you still wish to go back to the main menu?";
        }
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Chess.isGameOver = false;
                        Intent intent = new Intent(ChessActivity.this, MainActivity.class);
                        ChessActivity.this.startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                })
                .setIcon(R.drawable.logo)
                .show();
    }

    public void showToast(String s) {
        Context context = getApplicationContext();
        CharSequence text = s;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    /**
     * Highlights tiles that piece can move to in GUI
     * @param row The row of the selected piece
     * @param col The column of the selected piece
     */
    public void showMovableTiles(int row, int col) {
        Piece p = chessBoard.board[row][col];
        p.pieceButton.getBackground().setColorFilter(Color.parseColor("#eeccff"), PorterDuff.Mode.DARKEN);
        if (p instanceof Pawn) {
            if (p.getColor() == 0) {
                //pawn moves forward
                if (row-1 >= 0 && chessBoard.board[row-1][col] instanceof Blank) {
                    chessBoard.board[row-1][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                }
                //pawn double move
                if (row == 6) {
                    if (row-2 >= 0 && chessBoard.board[row-2][col] instanceof Blank) {
                        chessBoard.board[row-2][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                    }
                }
                //pawn capture northwest
                if (row - 1 >= 0 && col-1 >= 0 && !(chessBoard.board[row-1][col-1] instanceof Blank) && chessBoard.board[row-1][col-1].getColor() != p.getColor()) {
                    chessBoard.board[row-1][col-1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
                //pawn capture northeast
                if (row - 1 >= 0 && col+1 <= 7 && !(chessBoard.board[row-1][col+1] instanceof Blank) && chessBoard.board[row-1][col+1].getColor() != p.getColor()) {
                    chessBoard.board[row-1][col+1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
                //En passant stuff
                if ((row-1 >= 0 && col-1 >= 0 && chessBoard.board[row-1][col-1] instanceof Blank
                         && chessBoard.board[row][col-1].getColor() != p.getColor() && chessBoard.board[row][col-1] == (Piece)Chess.recentlyDoubleMovedPawnQueue.peek())) {
                    chessBoard.board[row-1][col-1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
                if ((row-1 >= 0 && col+1 <= 7 && chessBoard.board[row-1][col+1] instanceof Blank
                        && chessBoard.board[row][col+1].getColor() != p.getColor() && chessBoard.board[row][col+1] == (Piece)Chess.recentlyDoubleMovedPawnQueue.peek())) {
                    chessBoard.board[row-1][col+1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }

            }
            else if (p.getColor() == 1) {
                if (row+1 <= 7 && chessBoard.board[row+1][col] instanceof Blank) {
                    chessBoard.board[row+1][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                }
                if (row == 1) {
                    if (row+2 <= 7 && chessBoard.board[row+2][col] instanceof Blank) {
                        chessBoard.board[row+2][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                    }
                }
                //pawn capture southwest
                if (row + 1 <= 7 && col-1 >= 0 && !(chessBoard.board[row+1][col-1] instanceof Blank) && chessBoard.board[row+1][col-1].getColor() != p.getColor()) {
                    chessBoard.board[row+1][col-1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
                //pawn capture southeast
                if (row + 1 <= 7 && col+1 <= 7 && !(chessBoard.board[row+1][col+1] instanceof Blank) && chessBoard.board[row+1][col+1].getColor() != p.getColor()) {
                    chessBoard.board[row+1][col+1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
                //En passant stuff
                if ((row+1 <= 7 && col-1 >= 0 && chessBoard.board[row+1][col-1] instanceof Blank
                        && chessBoard.board[row][col-1].getColor() != p.getColor() && chessBoard.board[row][col-1] == (Piece)Chess.recentlyDoubleMovedPawnQueue.peek())) {
                    chessBoard.board[row+1][col-1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
                if ((row+1 <= 7 && col+1 <= 7 && chessBoard.board[row+1][col+1] instanceof Blank
                        && chessBoard.board[row][col+1].getColor() != p.getColor() && chessBoard.board[row][col+1] == (Piece)Chess.recentlyDoubleMovedPawnQueue.peek())) {
                    chessBoard.board[row+1][col+1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }

            }
        }

        if (p instanceof Bishop || p instanceof Queen) {
            //moving northeast
            for (int i = row-1, j = col+1; i >= 0 && j <= 7; i--, j++) {
                if (chessBoard.board[i][j] instanceof Blank) {
                    chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    if (chessBoard.board[i][j].getColor() != p.getColor()) {
                        chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                    }
                    break;
                }
            }
            //moving northwest
            for (int i = row-1, j = col-1; i >= 0 && j >= 0; i--, j--) {
                if (chessBoard.board[i][j] instanceof Blank) {
                    chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    if (chessBoard.board[i][j].getColor() != p.getColor()) {
                        chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                    }
                    break;
                }
            }
            //moving southwest
            for (int i = row+1, j = col-1; i <= 7 && j >= 0; i++, j--) {
                if (chessBoard.board[i][j] instanceof Blank) {
                    chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    if (chessBoard.board[i][j].getColor() != p.getColor()) {
                        chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                    }
                    break;
                }
            }
            //moving southeast
            for (int i = row+1, j = col+1; i <= 7 && j <= 7; i++, j++) {
                if (chessBoard.board[i][j] instanceof Blank) {
                    chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    if (chessBoard.board[i][j].getColor() != p.getColor()) {
                        chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                    }
                    break;
                }
            }
        }
        if (p instanceof Rook || p instanceof Queen) {
            //If the rook attempts to move vertical; looks for pieces in between so it doesn't skip over them; white and black
            for (int i = row + 1; i <= 7; i++) {
                if (chessBoard.board[i][col] instanceof Blank)
                    chessBoard.board[i][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                else {
                    if (chessBoard.board[i][col].getColor() != p.getColor()) {
                        chessBoard.board[i][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                    }
                    break;
                }
            }
            for (int i = row - 1; i >= 0; i--) {
                if (chessBoard.board[i][col] instanceof Blank)
                    chessBoard.board[i][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                else {
                    if (chessBoard.board[i][col].getColor() != p.getColor()) {
                        chessBoard.board[i][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                    }
                    break;
                }
            }
            //If the rook attempts to move horizontal; looks for pieces in between so it doesn't skip over them; white and black
            for (int i = col + 1; i <= 7; i++) {
                if (chessBoard.board[row][i] instanceof Blank)
                    chessBoard.board[row][i].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                else {
                    if (chessBoard.board[row][i].getColor() != p.getColor()) {
                        chessBoard.board[row][i].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                    }
                    break;
                }
            }
            for (int i = col - 1; i >= 0; i--) {
                if (chessBoard.board[row][i] instanceof Blank)
                    chessBoard.board[row][i].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                else {
                    if (chessBoard.board[row][i].getColor() != p.getColor()) {
                        chessBoard.board[row][i].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                    }
                    break;
                }
            }
        }

        if (p instanceof Knight) {
            //check for knights of the old republic III, all 8 possible variations of where a knight can be placed in relation to a king.
            if (row + 1 < 8 && col + 2 < 8 && chessBoard.board[row + 1][col + 2].getColor() != p.getColor()) {
                if (chessBoard.board[row + 1][col + 2] instanceof Blank) {
                    chessBoard.board[row + 1][col + 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row + 1][col + 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row + 1 < 8 && col - 2 >= 0 && chessBoard.board[row + 1][col - 2].getColor() != p.getColor()) {
                if (chessBoard.board[row + 1][col - 2] instanceof Blank) {
                    chessBoard.board[row + 1][col - 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row + 1][col - 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row + 2 < 8 && col + 1 < 8 && chessBoard.board[row + 2][col + 1].getColor() != p.getColor()) {
                if (chessBoard.board[row + 2][col + 1] instanceof Blank) {
                    chessBoard.board[row + 2][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row + 2][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row + 2 < 8 && col - 1 >= 0 && chessBoard.board[row + 2][col - 1].getColor() != p.getColor()) {
                if (chessBoard.board[row + 2][col - 1] instanceof Blank) {
                    chessBoard.board[row + 2][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row + 2][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row - 1 >= 0 && col + 2 < 8 && chessBoard.board[row - 1][col + 2].getColor() != p.getColor()) {
                if (chessBoard.board[row - 1][col + 2] instanceof Blank) {
                    chessBoard.board[row - 1][col + 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row - 1][col + 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row - 1 >= 0 && col - 2 >= 0 && chessBoard.board[row - 1][col - 2].getColor() != p.getColor()) {
                if (chessBoard.board[row - 1][col - 2] instanceof Blank) {
                    chessBoard.board[row - 1][col - 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row - 1][col - 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row - 2 >= 0 && col + 1 < 8 && chessBoard.board[row - 2][col + 1].getColor() != p.getColor()) {
                if (chessBoard.board[row - 2][col + 1] instanceof Blank) {
                    chessBoard.board[row - 2][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row - 2][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row - 2 >= 0 && col - 1 >= 0 && chessBoard.board[row - 2][col - 1].getColor() != p.getColor()) {
                if (chessBoard.board[row - 2][col - 1] instanceof Blank) {
                    chessBoard.board[row - 2][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row - 2][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }
            //end knight checks
        }
        
        //FOR KING AND COUNTRY!!!!!!
        if (p instanceof King) {
            if (p.hasMoved == false && chessBoard.board[row][col+1] instanceof Blank && chessBoard.board[row][col+2] instanceof Blank
                    && chessBoard.board[row][col+3] instanceof Rook && chessBoard.board[row][col+3].hasMoved == false) {
                chessBoard.board[row][col + 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
            }
            if (p.hasMoved == false && chessBoard.board[row][col-1] instanceof Blank && chessBoard.board[row][col-2] instanceof Blank
                    && chessBoard.board[row][col-3] instanceof Blank && chessBoard.board[row][col-4] instanceof Rook
                    && chessBoard.board[row][col-4].hasMoved == false) {
                chessBoard.board[row][col - 2].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
            }
            
            if (row - 1 >= 0 && col - 1 >= 0 && chessBoard.board[row - 1][col - 1].getColor() != p.getColor()) {
                if (chessBoard.board[row - 1][col - 1] instanceof Blank) {
                    chessBoard.board[row - 1][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row - 1][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row - 1 >= 0 && chessBoard.board[row - 1][col].getColor() != p.getColor()) {
                if (chessBoard.board[row - 1][col] instanceof Blank) {
                    chessBoard.board[row - 1][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row - 1][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row - 1 >= 0 && col + 1 < 8 && chessBoard.board[row - 1][col + 1].getColor() != p.getColor()) {
                if (chessBoard.board[row - 1][col + 1] instanceof Blank) {
                    chessBoard.board[row - 1][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row - 1][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (col - 1 >= 0 && chessBoard.board[row][col - 1].getColor() != p.getColor()) {
                if (chessBoard.board[row][col - 1] instanceof Blank) {
                    chessBoard.board[row][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (col + 1 < 8 && chessBoard.board[row][col + 1].getColor() != p.getColor()) {
                if (chessBoard.board[row][col + 1] instanceof Blank) {
                    chessBoard.board[row][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row + 1 < 8 && col - 1 >= 0 && chessBoard.board[row + 1][col - 1].getColor() != p.getColor()) {
                if (chessBoard.board[row + 1][col - 1] instanceof Blank) {
                    chessBoard.board[row + 1][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row + 1][col - 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row + 1 < 8 && chessBoard.board[row + 1][col].getColor() != p.getColor()) {
                if (chessBoard.board[row + 1][col] instanceof Blank) {
                    chessBoard.board[row + 1][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row + 1][col].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }

            if (row + 1 < 8 && col + 1 < 8 && chessBoard.board[row + 1][col + 1].getColor() != p.getColor()) {
                if (chessBoard.board[row + 1][col + 1] instanceof Blank) {
                    chessBoard.board[row + 1][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#3399ff"), PorterDuff.Mode.DARKEN);
                } else {
                    chessBoard.board[row + 1][col + 1].pieceButton.getBackground().setColorFilter(Color.parseColor("#ff4d4d"), PorterDuff.Mode.DARKEN);
                }
            }
        }
        //end king checks
        
    }

    public void hideMovableTiles() {
        for (int i = 0; i < chessBoard.board.length; i++) {
            for (int j = 0; j < chessBoard.board[i].length; j++) {
                if (chessBoard.board[i][j] instanceof Blank) {
                    if (i % 2 == 0) {
                        if (j % 2 == 0) {
                            chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#FBC99F"), PorterDuff.Mode.DARKEN);
                        } else {
                            chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#D18B47"), PorterDuff.Mode.DARKEN);
                        }
                    } else {
                        if (j % 2 == 0) {
                            chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#D18B47"), PorterDuff.Mode.DARKEN);
                        } else {
                            chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#FBC99F"), PorterDuff.Mode.DARKEN);
                        }
                    }
                } else {
                    chessBoard.board[i][j].pieceButton.getBackground().setColorFilter(Color.parseColor("#d9d9d9"), PorterDuff.Mode.DARKEN);
                }
            }
        }
    }

    public static int dpToPx(int dp, Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }
}
