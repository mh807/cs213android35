package ericandmignhe.cs213android35;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Eric on 4/30/2017.
 */

public class Recording implements Serializable {

    public static ArrayList<Recording> recordingsList = new ArrayList<>();

    public static String storeDir = "dat";
    public static final String storeFile = "recordings.dat";

    public String name;

    public String date;


    public ArrayList<int[]> moveList;

    public Recording (String n, String d, ArrayList<int[]> moves) {
        name = n;
        date = d;
        moveList = moves;
    }

    public static void writeRecordings(ArrayList<Recording> rl) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storeDir + File.separator + storeFile));
        oos.writeObject(rl);
        oos.close();
    }

    public static void readRecordings() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storeDir+ File.separator + storeFile));
        recordingsList = (ArrayList<Recording>) ois.readObject();
        ois.close();
    }
}
